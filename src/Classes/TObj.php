<?php

namespace Rohama\Telegram\Type;

class TObj
{
    protected array $others;

    public function __construct(...$args)
    {
        $this->others = $args;
    }

    public function toArray(): array
    {
        $data = get_object_vars($this);
        unset($data['others']);
        $data = t_toArray($data);

        return array_filter(array_merge($this->others, $data));
    }

    public function toString(): string
    {
        return json_encode($this->toArray());
    }

    public function __serialize(): array
    {
        return $this->toArray();
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
