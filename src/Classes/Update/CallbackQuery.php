<?php

namespace Rohama\Telegram\Type\Update;

use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\Messages\MaybeInaccessibleMessage;
use Rohama\Telegram\Type\TObj;

class CallbackQuery extends TObj
{
    public User $from;

    public ?MaybeInaccessibleMessage $message;

    public function __construct(public string $id,
        array|User $from,
        array|MaybeInaccessibleMessage|null $message = null,
        public ?string $inline_message_id = null,
        public ?string $chat_instance = null,
        public ?string $data = null,
        public ?string $game_short_name = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->from = is_array($from) ? new User(...$from) : $from;
        $this->message = is_array($message) ? new MaybeInaccessibleMessage(...$message) : $message;
    }
}
