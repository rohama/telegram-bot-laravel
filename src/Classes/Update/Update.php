<?php

namespace Rohama\Telegram\Type\Update;

use Rohama\Telegram\Type\Chats\Business\BusinessConnection;
use Rohama\Telegram\Type\Events\BusinessMessagesDeleted;
use Rohama\Telegram\Type\Events\ChatBoostRemoved;
use Rohama\Telegram\Type\Events\ChatBoostUpdated;
use Rohama\Telegram\Type\Events\ChatJoinRequest;
use Rohama\Telegram\Type\Events\ChatMemberUpdated;
use Rohama\Telegram\Type\Events\MessageReactionCountUpdated;
use Rohama\Telegram\Type\Events\MessageReactionUpdated;
use Rohama\Telegram\Type\InlineQuery\ChosenInlineResult;
use Rohama\Telegram\Type\InlineQuery\InlineQuery;
use Rohama\Telegram\Type\Messages\Poll\Poll;
use Rohama\Telegram\Type\Messages\Poll\PollAnswer;
use Rohama\Telegram\Type\Payment\PreCheckoutQuery;
use Rohama\Telegram\Type\Payment\ShippingQuery;
use Rohama\Telegram\Type\TObj;

class Update extends TObj
{
    public ?Message $message;

    public ?Message $edited_message;

    public ?Message $channel_post;

    public ?Message $edited_channel_post;

    public ?BusinessConnection $business_connection;

    public ?Message $business_message;

    public ?Message $edited_business_message;

    public ?BusinessMessagesDeleted $deleted_business_messages;

    public ?MessageReactionUpdated $message_reaction;

    public ?MessageReactionCountUpdated $message_reaction_count;

    public ?InlineQuery $inline_query;

    public ?ChosenInlineResult $chosen_inline_result;

    public ?CallbackQuery $callback_query;

    public ?ShippingQuery $shipping_query;

    public ?PreCheckoutQuery $pre_checkout_query;

    public ?Poll $poll;

    public ?PollAnswer $poll_answer;

    public ?ChatMemberUpdated $my_chat_member;

    public ?ChatMemberUpdated $chat_member;

    public ?ChatJoinRequest $chat_join_request;

    public ?ChatBoostUpdated $chat_boost;

    public ?ChatBoostRemoved $removed_chat_boost;

    public function __construct(public int $update_id,
        array|Message|null $message = null,
        array|Message|null $edited_message = null,
        array|Message|null $channel_post = null,
        array|Message|null $edited_channel_post = null,
        array|BusinessConnection|null $business_connection = null,
        array|Message|null $business_message = null,
        array|Message|null $edited_business_message = null,
        array|BusinessMessagesDeleted|null $deleted_business_messages = null,
        array|MessageReactionUpdated|null $message_reaction = null,
        array|MessageReactionCountUpdated|null $message_reaction_count = null,
        array|InlineQuery|null $inline_query = null,
        array|ChosenInlineResult|null $chosen_inline_result = null,
        array|CallbackQuery|null $callback_query = null,
        array|ShippingQuery|null $shipping_query = null,
        array|PreCheckoutQuery|null $pre_checkout_query = null,
        array|Poll|null $poll = null,
        array|PollAnswer|null $poll_answer = null,
        array|ChatMemberUpdated|null $my_chat_member = null,
        array|ChatMemberUpdated|null $chat_member = null,
        array|ChatJoinRequest|null $chat_join_request = null,
        array|ChatBoostUpdated|null $chat_boost = null,
        array|ChatBoostRemoved|null $removed_chat_boost = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->message = is_array($message) ? new Message(...$message) : $message;
        $this->edited_message = is_array($edited_message) ? new Message(...$edited_message) : $edited_message;
        $this->channel_post = is_array($channel_post) ? new Message(...$channel_post) : $channel_post;
        $this->edited_channel_post = is_array($edited_channel_post) ? new Message(...$edited_channel_post) : $edited_channel_post;
        $this->business_connection = is_array($business_connection) ? new BusinessConnection(...$business_connection) : $business_connection;
        $this->business_message = is_array($business_message) ? new Message(...$business_message) : $business_message;
        $this->edited_business_message = is_array($edited_business_message) ? new Message(...$edited_business_message) : $edited_business_message;
        $this->deleted_business_messages = is_array($deleted_business_messages) ? new BusinessMessagesDeleted(...$deleted_business_messages) : $deleted_business_messages;
        $this->message_reaction = is_array($message_reaction) ? new MessageReactionUpdated(...$message_reaction) : $message_reaction;
        $this->message_reaction_count = is_array($message_reaction_count) ? new MessageReactionCountUpdated(...$message_reaction_count) : $message_reaction_count;
        $this->inline_query = is_array($inline_query) ? new InlineQuery(...$inline_query) : $inline_query;
        $this->chosen_inline_result = is_array($chosen_inline_result) ? new ChosenInlineResult(...$chosen_inline_result) : $chosen_inline_result;
        $this->callback_query = is_array($callback_query) ? new CallbackQuery(...$callback_query) : $callback_query;
        $this->shipping_query = is_array($shipping_query) ? new ShippingQuery(...$shipping_query) : $shipping_query;
        $this->pre_checkout_query = is_array($pre_checkout_query) ? new PreCheckoutQuery(...$pre_checkout_query) : $pre_checkout_query;
        $this->poll = is_array($poll) ? new Poll(...$poll) : $poll;
        $this->poll_answer = is_array($poll_answer) ? new PollAnswer(...$poll_answer) : $poll_answer;
        $this->my_chat_member = is_array($my_chat_member) ? new ChatMemberUpdated(...$my_chat_member) : $my_chat_member;
        $this->chat_member = is_array($chat_member) ? new ChatMemberUpdated(...$chat_member) : $chat_member;
        $this->chat_join_request = is_array($chat_join_request) ? new ChatJoinRequest(...$chat_join_request) : $chat_join_request;
        $this->chat_boost = is_array($chat_boost) ? new ChatBoostUpdated(...$chat_boost) : $chat_boost;
        $this->removed_chat_boost = is_array($removed_chat_boost) ? new ChatBoostRemoved(...$removed_chat_boost) : $removed_chat_boost;
    }
}
