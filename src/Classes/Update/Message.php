<?php

namespace Rohama\Telegram\Type\Update;

use Rohama\Telegram\Type\Chats\Background\ChatBackground;
use Rohama\Telegram\Type\Chats\Chat;
use Rohama\Telegram\Type\Chats\ChatShared;
use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\Chats\UsersShared;
use Rohama\Telegram\Type\Events\ChatBoostAdded;
use Rohama\Telegram\Type\Events\ForumTopicClosed;
use Rohama\Telegram\Type\Events\ForumTopicCreated;
use Rohama\Telegram\Type\Events\ForumTopicEdited;
use Rohama\Telegram\Type\Events\ForumTopicReopened;
use Rohama\Telegram\Type\Events\GeneralForumTopicHidden;
use Rohama\Telegram\Type\Events\GeneralForumTopicUnhidden;
use Rohama\Telegram\Type\Events\MessageAutoDeleteTimerChanged;
use Rohama\Telegram\Type\Events\ProximityAlertTriggered;
use Rohama\Telegram\Type\Events\VideoChatEnded;
use Rohama\Telegram\Type\Events\VideoChatParticipantsInvited;
use Rohama\Telegram\Type\Events\VideoChatScheduled;
use Rohama\Telegram\Type\Events\VideoChatStarted;
use Rohama\Telegram\Type\Events\WriteAccessAllowed;
use Rohama\Telegram\Type\Game\Game;
use Rohama\Telegram\Type\Messages\Contact;
use Rohama\Telegram\Type\Messages\Dice;
use Rohama\Telegram\Type\Messages\ExternalReplyInfo;
use Rohama\Telegram\Type\Messages\Giveaway\Giveaway;
use Rohama\Telegram\Type\Messages\Giveaway\GiveawayCompleted;
use Rohama\Telegram\Type\Messages\Giveaway\GiveawayCreated;
use Rohama\Telegram\Type\Messages\Giveaway\GiveawayWinners;
use Rohama\Telegram\Type\Messages\Keyboard\InlineKeyboardMarkup;
use Rohama\Telegram\Type\Messages\LinkPreviewOptions;
use Rohama\Telegram\Type\Messages\Location;
use Rohama\Telegram\Type\Messages\MaybeInaccessibleMessage;
use Rohama\Telegram\Type\Messages\Media\Animation;
use Rohama\Telegram\Type\Messages\Media\Audio;
use Rohama\Telegram\Type\Messages\Media\Document;
use Rohama\Telegram\Type\Messages\Media\PaidMediaInfo;
use Rohama\Telegram\Type\Messages\Media\PhotoSize;
use Rohama\Telegram\Type\Messages\Media\Story;
use Rohama\Telegram\Type\Messages\Media\Video;
use Rohama\Telegram\Type\Messages\Media\VideoNote;
use Rohama\Telegram\Type\Messages\Media\Voice;
use Rohama\Telegram\Type\Messages\MessageEntity;
use Rohama\Telegram\Type\Messages\MessageOrigin;
use Rohama\Telegram\Type\Messages\Poll\Poll;
use Rohama\Telegram\Type\Messages\TextQuote;
use Rohama\Telegram\Type\Messages\Venue;
use Rohama\Telegram\Type\Messages\WebAppData;
use Rohama\Telegram\Type\Passport\PassportData;
use Rohama\Telegram\Type\Payment\Invoice;
use Rohama\Telegram\Type\Payment\RefundedPayment;
use Rohama\Telegram\Type\Payment\SuccessfulPayment;
use Rohama\Telegram\Type\Sticker\Sticker;
use Rohama\Telegram\Type\TObj;

class Message extends TObj
{
    public ?User $from;

    public ?Chat $sender_chat;

    public Chat $chat;

    public ?User $sender_business_bot;

    public ?MessageOrigin $forward_origin;

    public ?Message $reply_to_message;

    public ?ExternalReplyInfo $external_reply;

    public ?TextQuote $quote;

    public ?Story $reply_to_story;

    public ?User $via_bot;

    public ?LinkPreviewOptions $link_preview_options;

    public ?InlineKeyboardMarkup $reply_markup;

    public ?array $entities;

    public ?array $caption_entities;

    public ?Animation $animation;

    public ?Audio $audio;

    public ?Document $document;

    public ?PaidMediaInfo $paid_media;

    public ?array $photo;

    public ?Sticker $sticker;

    public ?Story $story;

    public ?Video $video;

    public ?VideoNote $video_note;

    public ?Voice $voice;

    public ?Contact $contact;

    public ?Dice $dice;

    public ?Game $game;

    public ?Poll $poll;

    public ?Location $location;

    public ?Venue $venue;

    public ?MaybeInaccessibleMessage $pinned_message;

    public ?UsersShared $users_shared;

    public ?ChatShared $chat_shared;

    public ?WebAppData $web_app_data;

    public ?array $new_chat_members;

    public ?User $left_chat_member;

    public ?array $new_chat_photo;

    public ?MessageAutoDeleteTimerChanged $message_auto_delete_timer_changed;

    public ?Invoice $invoice;

    public ?SuccessfulPayment $successful_payment;

    public ?RefundedPayment $refunded_payment;

    public ?WriteAccessAllowed $write_access_allowed;

    public ?PassportData $passport_data;

    public ?ProximityAlertTriggered $proximity_alert_triggered;

    public ?ChatBoostAdded $boost_added;

    public ?ChatBackground $chat_background_set;

    public ?ForumTopicCreated $forum_topic_created;

    public ?ForumTopicEdited $forum_topic_edited;

    public ?ForumTopicClosed $forum_topic_closed;

    public ?ForumTopicReopened $forum_topic_reopened;

    public ?GeneralForumTopicHidden $general_forum_topic_hidden;

    public ?GeneralForumTopicUnhidden $general_forum_topic_unhidden;

    public ?GiveawayCreated $giveaway_created;

    public ?Giveaway $giveaway;

    public ?GiveawayWinners $giveaway_winners;

    public ?GiveawayCompleted $giveaway_completed;

    public ?VideoChatScheduled $video_chat_scheduled;

    public ?VideoChatStarted $video_chat_started;

    public ?VideoChatEnded $video_chat_ended;

    public ?VideoChatParticipantsInvited $video_chat_participants_invited;

    public function __construct(public int $message_id,
        array|Chat $chat,
        public int $date,
        public ?int $message_thread_id = null,
        public ?int $sender_boost_count = null,
        array|User|null $from = null,
        array|Chat|null $sender_chat = null,
        array|User|null $sender_business_bot = null,
        public ?string $business_connection_id = null,
        array|MessageOrigin|null $forward_origin = null,
        public ?bool $is_topic_message = null,
        public ?bool $is_automatic_forward = null,
        array|Message|null $reply_to_message = null,
        array|ExternalReplyInfo|null $external_reply = null,
        array|TextQuote|null $quote = null,
        array|Story|null $reply_to_story = null,
        array|User|null $via_bot = null,
        public ?int $edit_date = null,
        public ?string $text = null,
        public ?string $media_group_id = null,
        public ?bool $has_protected_content = null,
        public ?bool $is_from_offline = null,
        public ?bool $has_media_spoiler = null,
        public ?string $caption = null,
        public ?string $author_signature = null,
        array|LinkPreviewOptions|null $link_preview_options = null,
        public ?string $effect_id = null,
        ?array $entities = null,
        ?array $caption_entities = null,
        array|Animation|null $animation = null,
        array|Audio|null $audio = null,
        array|Document|null $document = null,
        array|PaidMediaInfo|null $paid_media = null,
        ?array $photo = null,
        array|Sticker|null $sticker = null,
        array|Story|null $story = null,
        array|Video|null $video = null,
        array|VideoNote|null $video_note = null,
        array|Voice|null $voice = null,
        public ?bool $show_caption_above_media = null,
        array|Contact|null $contact = null,
        array|Dice|null $dice = null,
        array|Game|null $game = null,
        array|Poll|null $poll = null,
        array|Location|null $location = null,
        array|Venue|null $venue = null,
        array|MaybeInaccessibleMessage|null $pinned_message = null,
        array|UsersShared|null $users_shared = null,
        array|ChatShared|null $chat_shared = null,
        ?array $new_chat_members = null,
        array|User|null $left_chat_member = null,
        public ?string $new_chat_title = null,
        ?array $new_chat_photo = null,
        public ?bool $delete_chat_photo = null,
        public ?bool $group_chat_created = null,
        public ?bool $supergroup_chat_created = null,
        public ?bool $channel_chat_created = null,
        array|MessageAutoDeleteTimerChanged|null $message_auto_delete_timer_changed = null,
        public ?int $migrate_to_chat_id = null,
        public ?int $migrate_from_chat_id = null,
        array|Invoice|null $invoice = null,
        array|SuccessfulPayment|null $successful_payment = null,
        array|RefundedPayment|null $refunded_payment = null,
        public ?string $connected_website = null,
        array|WriteAccessAllowed|null $write_access_allowed = null,
        array|PassportData|null $passport_data = null,
        array|ProximityAlertTriggered|null $proximity_alert_triggered = null,
        array|ChatBoostAdded|null $boost_added = null,
        array|ChatBackground|null $chat_background_set = null,
        array|ForumTopicCreated|null $forum_topic_created = null,
        array|ForumTopicEdited|null $forum_topic_edited = null,
        array|ForumTopicClosed|null $forum_topic_closed = null,
        array|ForumTopicReopened|null $forum_topic_reopened = null,
        array|GeneralForumTopicHidden|null $general_forum_topic_hidden = null,
        array|GeneralForumTopicUnhidden|null $general_forum_topic_unhidden = null,
        array|GiveawayCreated|null $giveaway_created = null,
        array|Giveaway|null $giveaway = null,
        array|GiveawayWinners|null $giveaway_winners = null,
        array|GiveawayCompleted|null $giveaway_completed = null,
        array|VideoChatScheduled|null $video_chat_scheduled = null,
        array|VideoChatStarted|null $video_chat_started = null,
        array|VideoChatEnded|null $video_chat_ended = null,
        array|VideoChatParticipantsInvited|null $video_chat_participants_invited = null,
        array|WebAppData|null $web_app_data = null,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->from = is_array($from) ? new User(...$from) : $from;
        $this->sender_chat = is_array($sender_chat) ? new Chat(...$sender_chat) : $sender_chat;
        $this->sender_business_bot = is_array($sender_business_bot) ? new User(...$sender_business_bot) : $sender_business_bot;
        $this->chat = is_array($chat) ? new Chat(...$chat) : $chat;
        $this->forward_origin = is_array($forward_origin) ? new MessageOrigin(...$forward_origin) : $forward_origin;
        $this->reply_to_message = is_array($reply_to_message) ? new Message(...$reply_to_message) : $reply_to_message;
        $this->external_reply = is_array($external_reply) ? new ExternalReplyInfo(...$external_reply) : $external_reply;
        $this->quote = is_array($quote) ? new TextQuote(...$quote) : $quote;
        $this->reply_to_story = is_array($reply_to_story) ? new Story(...$reply_to_story) : $reply_to_story;
        $this->via_bot = is_array($via_bot) ? new User(...$via_bot) : $via_bot;
        $this->entities = is_null($entities) ? null : array_map(function ($entity) {
            return is_array($entity) ? new MessageEntity(...$entity) : $entity;
        }, $entities);
        $this->link_preview_options = is_array($link_preview_options) ? new LinkPreviewOptions(...$link_preview_options) : $link_preview_options;
        $this->animation = is_array($animation) ? new Animation(...$animation) : $animation;
        $this->audio = is_array($audio) ? new Audio(...$audio) : $audio;
        $this->document = is_array($document) ? new Document(...$document) : $document;
        $this->paid_media = is_array($paid_media) ? new PaidMediaInfo(...$paid_media) : $paid_media;
        $this->photo = is_null($photo) ? null : array_map(function ($photo_size) {
            return is_array($photo_size) ? new PhotoSize(...$photo_size) : $photo_size;
        }, $photo);
        $this->sticker = is_array($sticker) ? new Sticker(...$sticker) : $sticker;
        $this->story = is_array($story) ? new Story(...$story) : $story;
        $this->video = is_array($video) ? new Video(...$video) : $video;
        $this->video_note = is_array($video_note) ? new VideoNote(...$video_note) : $video_note;
        $this->voice = is_array($voice) ? new Voice(...$voice) : $voice;
        $this->caption_entities = is_null($caption_entities) ? null : array_map(function ($entity) {
            return is_array($entity) ? new MessageEntity(...$entity) : $entity;
        }, $caption_entities);
        $this->contact = is_array($contact) ? new Contact(...$contact) : $contact;
        $this->dice = is_array($dice) ? new Dice(...$dice) : $dice;
        $this->game = is_array($game) ? new Game(...$game) : $game;
        $this->poll = is_array($poll) ? new Poll(...$poll) : $poll;
        $this->venue = is_array($venue) ? new Venue(...$venue) : $venue;
        $this->location = is_array($location) ? new Location(...$location) : $location;
        $this->new_chat_members = is_null($new_chat_members) ? null : array_map(function ($new_chat_member) {
            return is_array($new_chat_member) ? new User(...$new_chat_member) : $new_chat_member;
        }, $new_chat_members);
        $this->left_chat_member = is_array($left_chat_member) ? new User(...$left_chat_member) : $left_chat_member;
        $this->new_chat_photo = is_null($new_chat_photo) ? null : array_map(function ($photo_size) {
            return is_array($photo_size) ? new PhotoSize(...$photo_size) : $photo_size;
        }, $new_chat_photo);
        $this->message_auto_delete_timer_changed = is_array($message_auto_delete_timer_changed) ? new MessageAutoDeleteTimerChanged(...$message_auto_delete_timer_changed) : $message_auto_delete_timer_changed;
        $this->pinned_message = is_array($pinned_message) ? new MaybeInaccessibleMessage(...$pinned_message) : $pinned_message;
        $this->invoice = is_array($invoice) ? new Invoice(...$invoice) : $invoice;
        $this->successful_payment = is_array($successful_payment) ? new SuccessfulPayment(...$successful_payment) : $successful_payment;
        $this->refunded_payment = is_array($refunded_payment) ? new RefundedPayment(...$refunded_payment) : $refunded_payment;
        $this->users_shared = is_array($users_shared) ? new UsersShared(...$users_shared) : $users_shared;
        $this->chat_shared = is_array($chat_shared) ? new ChatShared(...$chat_shared) : $chat_shared;
        $this->write_access_allowed = is_array($write_access_allowed) ? new WriteAccessAllowed(...$write_access_allowed) : $write_access_allowed;
        $this->passport_data = is_array($passport_data) ? new PassportData(...$passport_data) : $passport_data;
        $this->proximity_alert_triggered = is_array($proximity_alert_triggered) ? new ProximityAlertTriggered(...$proximity_alert_triggered) : $proximity_alert_triggered;
        $this->boost_added = is_array($boost_added) ? new ChatBoostAdded(...$boost_added) : $boost_added;
        $this->chat_background_set = is_array($chat_background_set) ? new ChatBackground(...$chat_background_set) : $chat_background_set;
        $this->forum_topic_created = is_array($forum_topic_created) ? new ForumTopicCreated(...$forum_topic_created) : $forum_topic_created;
        $this->forum_topic_edited = is_array($forum_topic_edited) ? new ForumTopicEdited(...$forum_topic_edited) : $forum_topic_edited;
        $this->forum_topic_closed = is_array($forum_topic_closed) ? new ForumTopicClosed(...$forum_topic_closed) : $forum_topic_closed;
        $this->forum_topic_reopened = is_array($forum_topic_reopened) ? new ForumTopicReopened(...$forum_topic_reopened) : $forum_topic_reopened;
        $this->general_forum_topic_hidden = is_array($general_forum_topic_hidden) ? new GeneralForumTopicHidden(...$general_forum_topic_hidden) : $general_forum_topic_hidden;
        $this->general_forum_topic_unhidden = is_array($general_forum_topic_unhidden) ? new GeneralForumTopicUnhidden(...$general_forum_topic_unhidden) : $general_forum_topic_unhidden;
        $this->giveaway_created = is_array($giveaway_created) ? new GiveawayCreated(...$giveaway_created) : $giveaway_created;
        $this->giveaway = is_array($giveaway) ? new Giveaway(...$giveaway) : $giveaway;
        $this->giveaway_winners = is_array($giveaway_winners) ? new GiveawayWinners(...$giveaway_winners) : $giveaway_winners;
        $this->giveaway_completed = is_array($giveaway_completed) ? new GiveawayCompleted(...$giveaway_completed) : $giveaway_completed;
        $this->video_chat_scheduled = is_array($video_chat_scheduled) ? new VideoChatScheduled(...$video_chat_scheduled) : $video_chat_scheduled;
        $this->video_chat_started = is_array($video_chat_started) ? new VideoChatStarted(...$video_chat_started) : $video_chat_started;
        $this->video_chat_ended = is_array($video_chat_ended) ? new VideoChatEnded(...$video_chat_ended) : $video_chat_ended;
        $this->video_chat_participants_invited = is_array($video_chat_participants_invited) ? new VideoChatParticipantsInvited(...$video_chat_participants_invited) : $video_chat_participants_invited;
        $this->web_app_data = is_array($web_app_data) ? new WebAppData(...$web_app_data) : $web_app_data;
        $this->reply_markup = is_array($reply_markup) ? new InlineKeyboardMarkup(...$reply_markup) : $reply_markup;
    }
}
