<?php

namespace Rohama\Telegram\Type\InlineQuery;

use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\Messages\LinkPreviewOptions;
use Rohama\Telegram\Type\Messages\MessageEntity;
use Rohama\Telegram\Type\Payment\LabeledPrice;
use Rohama\Telegram\Type\TObj;

class InputMessageContent extends TObj
{
    public ?array $entities;

    public ?array $prices;

    public ?LinkPreviewOptions $link_preview_options;

    public function __construct(public ?string $message_text = null,
        public ?string $parse_mode = null,
        ?array $entities = null,
        array|LinkPreviewOptions|null $link_preview_options = null,
        public ?float $latitude = null,
        public ?float $longitude = null,
        public ?float $horizontal_accuracy = null,
        public ?int $live_period = null,
        public ?int $heading = null,
        public ?int $proximity_alert_radius = null,
        public ?string $title = null,
        public ?string $address = null,
        public ?string $foursquare_id = null,
        public ?string $foursquare_type = null,
        public ?string $google_place_id = null,
        public ?string $google_place_type = null,
        public ?string $phone_number = null,
        public ?string $first_name = null,
        public ?string $last_name = null,
        public ?string $vcard = null,
        public ?string $description = null,
        public ?string $payload = null,
        public ?string $provider_token = null,
        public ?string $currency = null,
        ?array $prices = null,
        public ?int $max_tip_amount = null,
        public ?array $suggested_tip_amounts = null,
        public ?string $provider_data = null,
        public ?string $photo_url = null,
        public ?int $photo_size = null,
        public ?int $photo_width = null,
        public ?int $photo_height = null,
        public ?bool $need_name = null,
        public ?bool $need_phone_number = null,
        public ?bool $need_email = null,
        public ?bool $need_shipping_address = null,
        public ?bool $send_phone_number_to_provider = null,
        public ?bool $send_email_to_provider = null,
        public ?bool $is_flexible = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->link_preview_options = is_array($link_preview_options) ? new User(...$link_preview_options) : $link_preview_options;
        $this->entities = is_array($entities) ? array_map(function ($entity) {
            return is_array($entity) ? new MessageEntity(...$entity) : $entity;
        }, $entities) : $entities;
        $this->prices = is_array($prices) ? array_map(function ($price) {
            return is_array($price) ? new LabeledPrice(...$price) : $price;
        }, $prices) : $prices;
    }

    public static function InputTextMessageContent(string $message_text,
        ?string $parse_mode = null,
        ?array $entities = null,
        array|LinkPreviewOptions|null $link_preview_options = null): self
    {
        return new InputMessageContent(...[
            'message_text' => $message_text,
            'parse_mode' => $parse_mode,
            'entities' => $entities,
            'link_preview_options' => $link_preview_options,
        ]);
    }

    public static function InputLocationMessageContent(float $latitude,
        float $longitude,
        ?float $horizontal_accuracy = null,
        ?int $live_period = null,
        ?int $heading = null,
        ?int $proximity_alert_radius = null): self
    {
        return new InputMessageContent(...[
            'latitude' => $latitude,
            'longitude' => $longitude,
            'horizontal_accuracy' => $horizontal_accuracy,
            'live_period' => $live_period,
            'heading' => $heading,
            'proximity_alert_radius' => $proximity_alert_radius,
        ]);
    }

    public static function InputVenueMessageContent(float $latitude,
        float $longitude,
        string $title,
        string $address,
        ?string $foursquare_id = null,
        ?string $foursquare_type = null,
        ?string $google_place_id = null,
        ?string $google_place_type = null): self
    {
        return new InputMessageContent(...[
            'latitude' => $latitude,
            'longitude' => $longitude,
            'title' => $title,
            'address' => $address,
            'foursquare_id' => $foursquare_id,
            'foursquare_type' => $foursquare_type,
            'google_place_id' => $google_place_id,
            'google_place_type' => $google_place_type,
        ]);
    }

    public static function InputContactMessageContent(string $phone_number,
        string $first_name,
        ?string $last_name = null,
        ?string $vcard = null): self
    {
        return new InputMessageContent(...[
            'phone_number' => $phone_number,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'vcard' => $vcard,
        ]);
    }

    public static function InputInvoiceMessageContent(string $title,
        string $description,
        string $payload,
        string $currency,
        array $prices,
        ?string $provider_token = null,
        ?int $max_tip_amount = null,
        ?array $suggested_tip_amounts = null,
        ?string $provider_data = null,
        ?string $photo_url = null,
        ?int $photo_size = null,
        ?int $photo_width = null,
        ?int $photo_height = null,
        ?bool $need_name = null,
        ?bool $need_phone_number = null,
        ?bool $need_email = null,
        ?bool $need_shipping_address = null,
        ?bool $send_phone_number_to_provider = null,
        ?bool $send_email_to_provider = null,
        ?bool $is_flexible = null): self
    {
        return new InputMessageContent(...[
            'title' => $title,
            'description' => $description,
            'payload' => $payload,
            'currency' => $currency,
            'prices' => $prices,
            'provider_token' => $provider_token,
            'max_tip_amount' => $max_tip_amount,
            'suggested_tip_amounts' => $suggested_tip_amounts,
            'provider_data' => $provider_data,
            'photo_url' => $photo_url,
            'photo_size' => $photo_size,
            'photo_width' => $photo_width,
            'photo_height' => $photo_height,
            'need_name' => $need_name,
            'need_phone_number' => $need_phone_number,
            'need_email' => $need_email,
            'need_shipping_address' => $need_shipping_address,
            'send_phone_number_to_provider' => $send_phone_number_to_provider,
            'send_email_to_provider' => $send_email_to_provider,
            'is_flexible' => $is_flexible,
        ]);
    }
}
