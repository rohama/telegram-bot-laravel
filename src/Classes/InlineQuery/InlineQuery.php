<?php

namespace Rohama\Telegram\Type\InlineQuery;

use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\Messages\Location;
use Rohama\Telegram\Type\TObj;

class InlineQuery extends TObj
{
    public User $from;

    public ?Location $credentials;

    public function __construct(public string $id,
        array|User $from,
        public string $query,
        public string $offset,
        public ?string $chat_type = null,
        array|Location|null $location = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->from = is_array($from) ? new User(...$from) : $from;
        $this->location = is_array($location) ? new Location(...$location) : $location;
    }
}
