<?php

namespace Rohama\Telegram\Type\InlineQuery;

use Rohama\Telegram\Type\Messages\Keyboard\InlineKeyboardMarkup;
use Rohama\Telegram\Type\Messages\MessageEntity;
use Rohama\Telegram\Type\TObj;

class InlineQueryResult extends TObj
{
    public ?InputMessageContent $input_message_content;

    public ?InlineKeyboardMarkup $reply_markup;

    public ?array $caption_entities;

    public function __construct(public string $type,
        public string $id,
        public ?string $title = null,
        array|InputMessageContent|null $input_message_content = null,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        public ?string $url = null,
        public ?bool $hide_url = null,
        public ?string $description = null,
        public ?string $thumbnail_url = null,
        public ?int $thumbnail_width = null,
        public ?int $thumbnail_height = null,
        public ?string $photo_url = null,
        public ?int $photo_width = null,
        public ?int $photo_height = null,
        public ?string $caption = null,
        public ?string $parse_mode = null,
        ?array $caption_entities = null,
        public ?bool $show_caption_above_media = null,
        public ?string $gif_url = null,
        public ?int $gif_width = null,
        public ?int $gif_height = null,
        public ?int $gif_duration = null,
        public ?string $thumbnail_mime_type = null,
        public ?string $mpeg4_url = null,
        public ?int $mpeg4_width = null,
        public ?int $mpeg4_height = null,
        public ?int $mpeg4_duration = null,
        public ?string $video_url = null,
        public ?string $mime_type = null,
        public ?int $video_width = null,
        public ?int $video_height = null,
        public ?int $video_duration = null,
        public ?string $audio_url = null,
        public ?string $performer = null,
        public ?int $audio_duration = null,
        public ?string $voice_url = null,
        public ?int $voice_duration = null,
        public ?string $document_url = null,
        public ?float $latitude = null,
        public ?float $longitude = null,
        public ?float $horizontal_accuracy = null,
        public ?int $live_period = null,
        public ?int $heading = null,
        public ?int $proximity_alert_radius = null,
        public ?string $address = null,
        public ?string $foursquare_id = null,
        public ?string $foursquare_type = null,
        public ?string $google_place_id = null,
        public ?string $google_place_type = null,
        public ?string $phone_number = null,
        public ?string $first_name = null,
        public ?string $last_name = null,
        public ?string $vcard = null,
        public ?string $game_short_name = null,
        public ?string $photo_file_id = null,
        public ?string $gif_file_id = null,
        public ?string $mpeg4_file_id = null,
        public ?string $sticker_file_id = null,
        public ?string $document_file_id = null,
        public ?string $video_file_id = null,
        public ?string $voice_file_id = null,
        public ?string $audio_file_id = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->input_message_content = is_array($input_message_content) ? new InputMessageContent(...$input_message_content) : $input_message_content;
        $this->reply_markup = is_array($reply_markup) ? new InlineKeyboardMarkup(...$reply_markup) : $reply_markup;
        $this->caption_entities = is_array($caption_entities) ? array_map(function ($entity) {
            return is_array($entity) ? new MessageEntity(...$entity) : $entity;
        }, $caption_entities) : $caption_entities;
    }

    public static function InlineQueryResultArticle(string $id,
        string $title,
        array|InputMessageContent $input_message_content,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        ?string $url = null,
        ?bool $hide_url = null,
        ?string $description = null,
        ?string $thumbnail_url = null,
        ?int $thumbnail_width = null,
        ?int $thumbnail_height = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'article',
            'id' => $id,
            'title' => $title,
            'input_message_content' => $input_message_content,
            'reply_markup' => $reply_markup,
            'url' => $url,
            'hide_url' => $hide_url,
            'description' => $description,
            'thumbnail_url' => $thumbnail_url,
            'thumbnail_width' => $thumbnail_width,
            'thumbnail_height' => $thumbnail_height,
        ]);
    }

    public static function InlineQueryResultPhoto(string $id,
        string $photo_url,
        string $thumbnail_url,
        ?int $photo_width = null,
        ?int $photo_height = null,
        ?string $title = null,
        ?string $description = null,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        ?bool $show_caption_above_media = null,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        array|InputMessageContent|null $input_message_content = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'photo',
            'id' => $id,
            'photo_url' => $photo_url,
            'thumbnail_url' => $thumbnail_url,
            'photo_width' => $photo_width,
            'photo_height' => $photo_height,
            'title' => $title,
            'description' => $description,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'show_caption_above_media' => $show_caption_above_media,
            'reply_markup' => $reply_markup,
            'input_message_content' => $input_message_content,
        ]);
    }

    public static function InlineQueryResultGif(string $id,
        string $gif_url,
        string $thumbnail_url,
        ?int $gif_width = null,
        ?int $gif_height = null,
        ?int $gif_duration = null,
        ?string $thumbnail_mime_type = null,
        ?string $title = null,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        ?bool $show_caption_above_media = null,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        array|InputMessageContent|null $input_message_content = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'gif',
            'id' => $id,
            'gif_url' => $gif_url,
            'thumbnail_url' => $thumbnail_url,
            'gif_width' => $gif_width,
            'gif_height' => $gif_height,
            'gif_duration' => $gif_duration,
            'thumbnail_mime_type' => $thumbnail_mime_type,
            'title' => $title,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'show_caption_above_media' => $show_caption_above_media,
            'reply_markup' => $reply_markup,
            'input_message_content' => $input_message_content,
        ]);
    }

    public static function InlineQueryResultMpeg4Gif(string $id,
        string $mpeg4_url,
        string $thumbnail_url,
        ?int $mpeg4_width = null,
        ?int $mpeg4_height = null,
        ?int $mpeg4_duration = null,
        ?string $thumbnail_mime_type = null,
        ?string $title = null,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        ?bool $show_caption_above_media = null,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        array|InputMessageContent|null $input_message_content = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'mpeg4_gif',
            'id' => $id,
            'mpeg4_url' => $mpeg4_url,
            'thumbnail_url' => $thumbnail_url,
            'mpeg4_width' => $mpeg4_width,
            'mpeg4_height' => $mpeg4_height,
            'mpeg4_duration' => $mpeg4_duration,
            'thumbnail_mime_type' => $thumbnail_mime_type,
            'title' => $title,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'show_caption_above_media' => $show_caption_above_media,
            'reply_markup' => $reply_markup,
            'input_message_content' => $input_message_content,
        ]);
    }

    public static function InlineQueryResultVideo(string $id,
        string $video_url,
        string $mime_type,
        string $thumbnail_url,
        string $title,
        ?int $video_width = null,
        ?int $video_height = null,
        ?int $video_duration = null,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        ?bool $show_caption_above_media = null,
        ?string $description = null,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        array|InputMessageContent|null $input_message_content = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'video',
            'id' => $id,
            'video_url' => $video_url,
            'mime_type' => $mime_type,
            'thumbnail_url' => $thumbnail_url,
            'video_width' => $video_width,
            'video_height' => $video_height,
            'video_duration' => $video_duration,
            'title' => $title,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'show_caption_above_media' => $show_caption_above_media,
            'description' => $description,
            'reply_markup' => $reply_markup,
            'input_message_content' => $input_message_content,
        ]);
    }

    public static function InlineQueryResultAudio(string $id,
        string $audio_url,
        string $title,
        ?string $performer = null,
        ?int $audio_duration = null,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        array|InputMessageContent|null $input_message_content = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'audio',
            'id' => $id,
            'audio_url' => $audio_url,
            'title' => $title,
            'performer' => $performer,
            'audio_duration' => $audio_duration,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'reply_markup' => $reply_markup,
            'input_message_content' => $input_message_content,
        ]);
    }

    public static function InlineQueryResultVoice(string $id,
        string $voice_url,
        string $title,
        ?int $voice_duration = null,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        array|InputMessageContent|null $input_message_content = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'voice',
            'id' => $id,
            'voice_url' => $voice_url,
            'title' => $title,
            'voice_duration' => $voice_duration,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'reply_markup' => $reply_markup,
            'input_message_content' => $input_message_content,
        ]);
    }

    public static function InlineQueryResultDocument(string $id,
        string $document_url,
        string $title,
        string $mime_type,
        ?string $description = null,
        ?string $thumbnail_url = null,
        ?int $thumbnail_width = null,
        ?int $thumbnail_height = null,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        array|InputMessageContent|null $input_message_content = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'document',
            'id' => $id,
            'document_url' => $document_url,
            'title' => $title,
            'mime_type' => $mime_type,
            'description' => $description,
            'thumbnail_url' => $thumbnail_url,
            'thumbnail_width' => $thumbnail_width,
            'thumbnail_height' => $thumbnail_height,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'reply_markup' => $reply_markup,
            'input_message_content' => $input_message_content,
        ]);
    }

    public static function InlineQueryResultLocation(string $id,
        float $latitude,
        float $longitude,
        string $title,
        ?float $horizontal_accuracy = null,
        ?int $proximity_alert_radius = null,
        ?int $heading = null,
        ?string $thumbnail_url = null,
        ?int $thumbnail_width = null,
        ?int $thumbnail_height = null,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        array|InputMessageContent|null $input_message_content = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'location',
            'id' => $id,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'title' => $title,
            'horizontal_accuracy' => $horizontal_accuracy,
            'proximity_alert_radius' => $proximity_alert_radius,
            'heading' => $heading,
            'thumbnail_url' => $thumbnail_url,
            'thumbnail_width' => $thumbnail_width,
            'thumbnail_height' => $thumbnail_height,
            'reply_markup' => $reply_markup,
            'input_message_content' => $input_message_content,
        ]);
    }

    public static function InlineQueryResultVenue(string $id,
        float $latitude,
        float $longitude,
        string $title,
        string $address,
        ?string $foursquare_id = null,
        ?string $foursquare_type = null,
        ?string $google_place_id = null,
        ?string $google_place_type = null,
        ?string $thumbnail_url = null,
        ?int $thumbnail_width = null,
        ?int $thumbnail_height = null,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        array|InputMessageContent|null $input_message_content = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'venue',
            'id' => $id,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'title' => $title,
            'address' => $address,
            'foursquare_id' => $foursquare_id,
            'foursquare_type' => $foursquare_type,
            'google_place_id' => $google_place_id,
            'google_place_type' => $google_place_type,
            'thumbnail_url' => $thumbnail_url,
            'thumbnail_width' => $thumbnail_width,
            'thumbnail_height' => $thumbnail_height,
            'reply_markup' => $reply_markup,
            'input_message_content' => $input_message_content,
        ]);
    }

    public static function InlineQueryResultContact(string $id,
        string $phone_number,
        string $first_name,
        ?string $last_name = null,
        ?string $vcard = null,
        ?string $thumbnail_url = null,
        ?int $thumbnail_width = null,
        ?int $thumbnail_height = null,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        array|InputMessageContent|null $input_message_content = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'contact',
            'id' => $id,
            'phone_number' => $phone_number,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'vcard' => $vcard,
            'thumbnail_url' => $thumbnail_url,
            'thumbnail_width' => $thumbnail_width,
            'thumbnail_height' => $thumbnail_height,
            'reply_markup' => $reply_markup,
            'input_message_content' => $input_message_content,
        ]);
    }

    public static function InlineQueryResultGame(string $id,
        string $game_short_name,
        array|InlineKeyboardMarkup|null $reply_markup = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'game',
            'id' => $id,
            'game_short_name' => $game_short_name,
            'reply_markup' => $reply_markup,
        ]);
    }

    public static function InlineQueryResultCachedPhoto(string $id,
        string $photo_file_id,
        ?string $title = null,
        ?string $description = null,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        ?bool $show_caption_above_media = null,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        array|InputMessageContent|null $input_message_content = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'photo',
            'id' => $id,
            'photo_file_id' => $photo_file_id,
            'title' => $title,
            'description' => $description,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'show_caption_above_media' => $show_caption_above_media,
            'reply_markup' => $reply_markup,
            'input_message_content' => $input_message_content,
        ]);
    }

    public static function InlineQueryResultCachedGif(string $id,
        string $gif_file_id,
        ?string $title = null,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        ?bool $show_caption_above_media = null,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        array|InputMessageContent|null $input_message_content = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'gif',
            'id' => $id,
            'gif_file_id' => $gif_file_id,
            'title' => $title,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'show_caption_above_media' => $show_caption_above_media,
            'reply_markup' => $reply_markup,
            'input_message_content' => $input_message_content,
        ]);
    }

    public static function InlineQueryResultCachedMpeg4Gif(string $id,
        string $mpeg4_file_id,
        ?string $title = null,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        ?bool $show_caption_above_media = null,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        array|InputMessageContent|null $input_message_content = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'mpeg4_gif',
            'id' => $id,
            'mpeg4_file_id' => $mpeg4_file_id,
            'title' => $title,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'show_caption_above_media' => $show_caption_above_media,
            'reply_markup' => $reply_markup,
            'input_message_content' => $input_message_content,
        ]);
    }

    public static function InlineQueryResultCachedSticker(string $id,
        string $sticker_file_id,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        array|InputMessageContent|null $input_message_content = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'sticker',
            'id' => $id,
            'sticker_file_id' => $sticker_file_id,
            'reply_markup' => $reply_markup,
            'input_message_content' => $input_message_content,
        ]);
    }

    public static function InlineQueryResultCachedDocument(string $id,
        string $document_file_id,
        string $title,
        ?string $description = null,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        array|InputMessageContent|null $input_message_content = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'document',
            'id' => $id,
            'document_file_id' => $document_file_id,
            'title' => $title,
            'description' => $description,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'reply_markup' => $reply_markup,
            'input_message_content' => $input_message_content,
        ]);
    }

    public static function InlineQueryResultCachedVideo(string $id,
        string $video_file_id,
        string $title,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        ?bool $show_caption_above_media = null,
        ?string $description = null,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        array|InputMessageContent|null $input_message_content = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'video',
            'id' => $id,
            'video_file_id' => $video_file_id,
            'title' => $title,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'show_caption_above_media' => $show_caption_above_media,
            'description' => $description,
            'reply_markup' => $reply_markup,
            'input_message_content' => $input_message_content,
        ]);
    }

    public static function InlineQueryResultCachedVoice(string $id,
        string $voice_file_id,
        string $title,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        array|InputMessageContent|null $input_message_content = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'voice',
            'id' => $id,
            'voice_file_id' => $voice_file_id,
            'title' => $title,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'reply_markup' => $reply_markup,
            'input_message_content' => $input_message_content,
        ]);
    }

    public static function InlineQueryResultCachedAudio(string $id,
        string $audio_file_id,
        string $title,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        array|InlineKeyboardMarkup|null $reply_markup = null,
        array|InputMessageContent|null $input_message_content = null): self
    {
        return new InlineQueryResult(...[
            'type' => 'audio',
            'id' => $id,
            'audio_file_id' => $audio_file_id,
            'title' => $title,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'reply_markup' => $reply_markup,
            'input_message_content' => $input_message_content,
        ]);
    }
}
