<?php

namespace Rohama\Telegram\Type\InlineQuery;

use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\Messages\Location;
use Rohama\Telegram\Type\TObj;

class ChosenInlineResult extends TObj
{
    public User $from;

    public ?Location $credentials;

    public function __construct(public string $result_id,
        array|User $from,
        array|Location|null $location = null,
        public ?string $inline_message_id = null,
        public ?string $query = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->from = is_array($from) ? new User(...$from) : $from;
        $this->location = is_array($location) ? new Location(...$location) : $location;
    }
}
