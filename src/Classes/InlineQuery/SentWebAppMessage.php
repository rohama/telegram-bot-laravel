<?php

namespace Rohama\Telegram\Type\InlineQuery;

use Rohama\Telegram\Type\TObj;

class SentWebAppMessage extends TObj
{
    public function __construct(public ?string $inline_message_id = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
