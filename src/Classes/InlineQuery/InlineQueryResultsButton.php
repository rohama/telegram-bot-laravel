<?php

namespace Rohama\Telegram\Type\InlineQuery;

use Rohama\Telegram\Type\Messages\WebAppInfo;
use Rohama\Telegram\Type\TObj;

class InlineQueryResultsButton extends TObj
{
    public ?WebAppInfo $web_app;

    public function __construct(public string $text,
        array|WebAppInfo|null $web_app,
        public ?string $start_parameter = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->web_app = is_array($web_app) ? new WebAppInfo(...$web_app) : $web_app;
    }
}
