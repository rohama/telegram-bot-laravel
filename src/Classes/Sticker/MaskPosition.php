<?php

namespace Rohama\Telegram\Type\Sticker;

use Rohama\Telegram\Type\TObj;

class MaskPosition extends TObj
{
    public function __construct(public string $point,
        public float $x_shift,
        public float $y_shift,
        public float $scale,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
