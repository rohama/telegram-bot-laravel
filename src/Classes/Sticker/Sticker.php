<?php

namespace Rohama\Telegram\Type\Sticker;

use Rohama\Telegram\Type\Messages\Media\File;
use Rohama\Telegram\Type\Messages\Media\Media;
use Rohama\Telegram\Type\Messages\Media\PhotoSize;

class Sticker extends Media
{
    public ?PhotoSize $thumbnail;

    public ?File $premium_animation;

    public ?MaskPosition $mask_position;

    public function __construct(public string $type,
        public int $width,
        public int $height,
        public bool $is_animated,
        public bool $is_video,
        array|PhotoSize|null $thumbnail = null,
        public ?string $emoji = null,
        public ?string $set_name = null,
        array|File|null $premium_animation = null,
        array|MaskPosition|null $mask_position = null,
        public ?string $custom_emoji_id = null,
        public ?bool $needs_repainting = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->thumbnail = is_array($thumbnail) ? new PhotoSize(...$thumbnail) : $thumbnail;
        $this->premium_animation = is_array($premium_animation) ? new File(...$premium_animation) : $premium_animation;
        $this->mask_position = is_array($mask_position) ? new MaskPosition(...$mask_position) : $mask_position;
    }
}
