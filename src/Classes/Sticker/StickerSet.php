<?php

namespace Rohama\Telegram\Type\Sticker;

use Rohama\Telegram\Type\Messages\Media\PhotoSize;
use Rohama\Telegram\Type\TObj;

class StickerSet extends TObj
{
    public array $stickers;

    public ?PhotoSize $thumbnail;

    public function __construct(public string $name,
        public string $title,
        public string $sticker_type,
        array $stickers,
        array|PhotoSize|null $thumbnail = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->thumbnail = is_array($thumbnail) ? new PhotoSize(...$thumbnail) : $thumbnail;
        $this->stickers = array_map(function ($sticker) {
            return is_array($sticker) ? new Sticker(...$sticker) : $sticker;
        }, $stickers);
    }
}
