<?php

namespace Rohama\Telegram\Type\Sticker;

use Rohama\Telegram\Type\InputFile;
use Rohama\Telegram\Type\Messages\Media\Media;

class InputSticker extends Media
{
    public ?MaskPosition $mask_position;

    public function __construct(public string|InputFile $sticker,
        public string $format,
        public array $emoji_list,
        array|MaskPosition|null $mask_position = null,
        public ?array $keywords = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->mask_position = is_array($mask_position) ? new MaskPosition(...$mask_position) : $mask_position;
    }
}
