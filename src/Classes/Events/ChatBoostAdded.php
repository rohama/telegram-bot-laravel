<?php

namespace Rohama\Telegram\Type\Events;

use Rohama\Telegram\Type\TObj;

class ChatBoostAdded extends TObj
{
    public function __construct(public int $boost_count,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
