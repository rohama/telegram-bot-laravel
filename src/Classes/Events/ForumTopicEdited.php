<?php

namespace Rohama\Telegram\Type\Events;

use Rohama\Telegram\Type\TObj;

class ForumTopicEdited extends TObj
{
    public function __construct(public ?string $name = null,
        public ?string $icon_custom_emoji_id = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
