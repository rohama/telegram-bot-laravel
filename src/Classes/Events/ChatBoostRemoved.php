<?php

namespace Rohama\Telegram\Type\Events;

use Rohama\Telegram\Type\Chats\Chat;
use Rohama\Telegram\Type\Chats\ChatBoostSource;
use Rohama\Telegram\Type\TObj;

class ChatBoostRemoved extends TObj
{
    public Chat $chat;

    public ChatBoostSource $source;

    public function __construct(array|Chat $chat,
        array|ChatBoostSource $source,
        public string $boost_id,
        public int $remove_date,
        ...$args)
    {
        parent::__construct(...$args);
        $this->chat = is_array($chat) ? new Chat(...$chat) : $chat;
        $this->source = is_array($source) ? new ChatBoostSource(...$source) : $source;
    }
}
