<?php

namespace Rohama\Telegram\Type\Events;

use Rohama\Telegram\Type\TObj;

class WriteAccessAllowed extends TObj
{
    public function __construct(public ?bool $from_request = null,
        public ?string $web_app_name = null,
        public ?bool $from_attachment_menu = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
