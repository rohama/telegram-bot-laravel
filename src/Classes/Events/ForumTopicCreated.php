<?php

namespace Rohama\Telegram\Type\Events;

use Rohama\Telegram\Type\TObj;

class ForumTopicCreated extends TObj
{
    public function __construct(public string $name,
        public int $icon_color,
        public ?string $icon_custom_emoji_id = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
