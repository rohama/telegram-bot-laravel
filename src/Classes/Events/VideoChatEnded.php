<?php

namespace Rohama\Telegram\Type\Events;

use Rohama\Telegram\Type\TObj;

class VideoChatEnded extends TObj
{
    public function __construct(public int $duration,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
