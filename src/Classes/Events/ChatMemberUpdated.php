<?php

namespace Rohama\Telegram\Type\Events;

use Rohama\Telegram\Type\Chats\Chat;
use Rohama\Telegram\Type\Chats\ChatInviteLink;
use Rohama\Telegram\Type\Chats\ChatMember;
use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\TObj;

class ChatMemberUpdated extends TObj
{
    public Chat $chat;

    public User $from;

    public ChatMember $old_chat_member;

    public ChatMember $new_chat_member;

    public ?ChatInviteLink $invite_link;

    public function __construct(array|Chat $chat,
        array|User $from,
        public int $date,
        array|ChatMember $old_chat_member,
        array|ChatMember $new_chat_member,
        array|ChatInviteLink|null $invite_link = null,
        public ?bool $via_join_request = null,
        public ?bool $via_chat_folder_invite_link = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->chat = is_array($chat) ? new Chat(...$chat) : $chat;
        $this->from = is_array($from) ? new User(...$from) : $from;
        $this->old_chat_member = is_array($old_chat_member) ? new ChatMember(...$old_chat_member) : $old_chat_member;
        $this->new_chat_member = is_array($new_chat_member) ? new ChatMember(...$new_chat_member) : $new_chat_member;
        $this->invite_link = is_array($invite_link) ? new ChatInviteLink(...$invite_link) : $invite_link;
    }
}
