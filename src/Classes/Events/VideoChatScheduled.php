<?php

namespace Rohama\Telegram\Type\Events;

use Rohama\Telegram\Type\TObj;

class VideoChatScheduled extends TObj
{
    public function __construct(public int $start_date,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
