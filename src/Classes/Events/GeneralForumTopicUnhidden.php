<?php

namespace Rohama\Telegram\Type\Events;

use Rohama\Telegram\Type\TObj;

class GeneralForumTopicUnhidden extends TObj
{
    public function __construct(...$args)
    {
        parent::__construct(...$args);
    }
}
