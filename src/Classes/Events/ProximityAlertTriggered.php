<?php

namespace Rohama\Telegram\Type\Events;

use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\TObj;

class ProximityAlertTriggered extends TObj
{
    public ?User $traveler;

    public ?User $watcher;

    public function __construct(public int $distance,
        array|User|null $traveler = null,
        array|User|null $watcher = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->traveler = is_array($traveler) ? new User(...$traveler) : $traveler;
        $this->watcher = is_array($watcher) ? new User(...$watcher) : $watcher;
    }
}
