<?php

namespace Rohama\Telegram\Type\Events;

use Rohama\Telegram\Type\Chats\Chat;
use Rohama\Telegram\Type\Chats\ChatInviteLink;
use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\TObj;

class ChatJoinRequest extends TObj
{
    public Chat $chat;

    public User $from;

    public ?ChatInviteLink $invite_link;

    public function __construct(array|Chat $chat,
        array|User $from,
        public int $date,
        public int $user_chat_id,
        array|ChatInviteLink|null $invite_link = null,
        public ?string $bio = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->chat = is_array($chat) ? new Chat(...$chat) : $chat;
        $this->from = is_array($from) ? new User(...$from) : $from;
        $this->invite_link = is_array($invite_link) ? new ChatInviteLink(...$invite_link) : $invite_link;
    }
}
