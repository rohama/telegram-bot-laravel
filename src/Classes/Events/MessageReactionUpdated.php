<?php

namespace Rohama\Telegram\Type\Events;

use Rohama\Telegram\Type\Chats\Chat;
use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\Messages\ReactionType;
use Rohama\Telegram\Type\TObj;

class MessageReactionUpdated extends TObj
{
    public Chat $chat;

    public ?User $user;

    public ?Chat $actor_chat;

    public array $old_reaction;

    public array $new_reaction;

    public function __construct(array|Chat $chat,
        public int $message_id,
        public int $date,
        array $old_reaction,
        array $new_reaction,
        array|User|null $user = null,
        array|Chat|null $actor_chat = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->chat = is_array($chat) ? new Chat(...$chat) : $chat;
        $this->actor_chat = is_array($actor_chat) ? new Chat(...$actor_chat) : $actor_chat;
        $this->user = is_array($user) ? new User(...$user) : $user;
        $this->old_reaction = array_map(function ($reaction) {
            return is_array($reaction) ? new ReactionType(...$reaction) : $reaction;
        }, $old_reaction);
        $this->new_reaction = array_map(function ($reaction) {
            return is_array($reaction) ? new ReactionType(...$reaction) : $reaction;
        }, $new_reaction);
    }
}
