<?php

namespace Rohama\Telegram\Type\Events;

use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\TObj;

class VideoChatParticipantsInvited extends TObj
{
    public array $users;

    public function __construct(array $users,
        ...$args)
    {
        parent::__construct(...$args);
        $this->users = array_map(function ($user) {
            return is_array($user) ? new User(...$user) : $user;
        }, $users);
    }
}
