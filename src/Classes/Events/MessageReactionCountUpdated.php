<?php

namespace Rohama\Telegram\Type\Events;

use Rohama\Telegram\Type\Chats\Chat;
use Rohama\Telegram\Type\Messages\ReactionCount;
use Rohama\Telegram\Type\TObj;

class MessageReactionCountUpdated extends TObj
{
    public Chat $chat;

    public array $reactions;

    public function __construct(array|Chat $chat,
        public int $message_id,
        public int $date,
        array $reactions,
        ...$args)
    {
        parent::__construct(...$args);
        $this->chat = is_array($chat) ? new Chat(...$chat) : $chat;
        $this->reactions = array_map(function ($reaction) {
            return is_array($reaction) ? new ReactionCount(...$reaction) : $reaction;
        }, $reactions);
    }
}
