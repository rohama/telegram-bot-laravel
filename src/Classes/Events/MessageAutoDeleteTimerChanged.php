<?php

namespace Rohama\Telegram\Type\Events;

use Rohama\Telegram\Type\TObj;

class MessageAutoDeleteTimerChanged extends TObj
{
    public function __construct(public int $message_auto_delete_time,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
