<?php

namespace Rohama\Telegram\Type\Events;

use Rohama\Telegram\Type\TObj;

class ForumTopicClosed extends TObj
{
    public function __construct(...$args)
    {
        parent::__construct(...$args);
    }
}
