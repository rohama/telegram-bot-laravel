<?php

namespace Rohama\Telegram\Type\Events;

use Rohama\Telegram\Type\Chats\Chat;
use Rohama\Telegram\Type\TObj;

class BusinessMessagesDeleted extends TObj
{
    public Chat $chat;

    public function __construct(array|Chat $chat,
        public string $business_connection_id,
        public array $message_ids,
        ...$args)
    {
        parent::__construct(...$args);
        $this->chat = is_array($chat) ? new Chat(...$chat) : $chat;
    }
}
