<?php

namespace Rohama\Telegram\Type\Events;

use Rohama\Telegram\Type\Chats\Chat;
use Rohama\Telegram\Type\Chats\ChatBoost;
use Rohama\Telegram\Type\TObj;

class ChatBoostUpdated extends TObj
{
    public Chat $chat;

    public ChatBoost $boost;

    public function __construct(array|Chat $chat,
        array|ChatBoost $boost,
        ...$args)
    {
        parent::__construct(...$args);
        $this->chat = is_array($chat) ? new Chat(...$chat) : $chat;
        $this->boost = is_array($boost) ? new ChatBoost(...$boost) : $boost;
    }
}
