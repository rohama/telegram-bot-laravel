<?php

namespace Rohama\Telegram\Type\Passport;

use Rohama\Telegram\Type\TObj;

class EncryptedCredentials extends TObj
{
    public function __construct(public string $data,
        public string $hash,
        public string $secret,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
