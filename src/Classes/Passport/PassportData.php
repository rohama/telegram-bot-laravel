<?php

namespace Rohama\Telegram\Type\Passport;

use Rohama\Telegram\Type\TObj;

class PassportData extends TObj
{
    public array $data;

    public EncryptedCredentials $credentials;

    public function __construct(array $data,
        array|EncryptedCredentials $credentials,
        ...$args)
    {
        parent::__construct(...$args);
        $this->credentials = is_array($credentials) ? new EncryptedCredentials(...$credentials) : $credentials;
        $this->data = array_map(function ($element) {
            return is_array($element) ? new EncryptedPassportElement(...$element) : $element;
        }, $data);
    }
}
