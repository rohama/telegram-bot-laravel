<?php

namespace Rohama\Telegram\Type\Passport;

use Rohama\Telegram\Type\TObj;

class EncryptedPassportElement extends TObj
{
    public ?array $files;

    public ?PassportFile $front_side;

    public ?PassportFile $reverse_side;

    public ?PassportFile $selfie;

    public ?array $translation;

    public function __construct(public string $type,
        public string $hash,
        public ?string $data = null,
        public ?string $phone_number = null,
        public ?string $email = null,
        ?array $files = null,
        array|PassportFile|null $front_side = null,
        array|PassportFile|null $reverse_side = null,
        array|PassportFile|null $selfie = null,
        ?array $translation = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->front_side = is_array($front_side) ? new PassportFile(...$front_side) : $front_side;
        $this->reverse_side = is_array($reverse_side) ? new PassportFile(...$reverse_side) : $reverse_side;
        $this->selfie = is_array($selfie) ? new PassportFile(...$selfie) : $selfie;
        $this->files = is_null($files) ? null : array_map(function ($file) {
            return is_array($file) ? new PassportFile(...$file) : $file;
        }, $files);
        $this->translation = is_null($translation) ? null : array_map(function ($file) {
            return is_array($file) ? new PassportFile(...$file) : $file;
        }, $translation);
    }
}
