<?php

namespace Rohama\Telegram\Type\Passport;

use Rohama\Telegram\Type\TObj;

class PassportFile extends TObj
{
    public function __construct(public string $file_id,
        public string $file_unique_id,
        public int $file_size,
        public int $file_date,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
