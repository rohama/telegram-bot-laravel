<?php

namespace Rohama\Telegram\Type\Passport;

use Rohama\Telegram\Type\TObj;

class PassportElementError extends TObj
{
    public function __construct(public string $source,
        public string $type,
        public string $message,
        public ?string $field_name = null,
        public ?string $data_hash = null,
        public ?string $file_hash = null,
        public ?array $file_hashes = null,
        public ?string $element_hash = null,
        ...$args)
    {
        parent::__construct(...$args);
    }

    public static function PassportElementErrorDataField(string $type,
        string $field_name,
        string $data_hash,
        string $message): self
    {
        return new PassportElementError(...[
            'source' => 'data',
            'type' => $type,
            'field_name' => $field_name,
            'data_hash' => $data_hash,
            'message' => $message,
        ]);
    }

    public static function PassportElementErrorFrontSide(string $type,
        string $file_hash,
        string $message): self
    {
        return new PassportElementError(...[
            'source' => 'front_side',
            'type' => $type,
            'file_hash' => $file_hash,
            'message' => $message,
        ]);
    }

    public static function PassportElementErrorReverseSide(string $type,
        string $file_hash,
        string $message): self
    {
        return new PassportElementError(...[
            'source' => 'reverse_side',
            'type' => $type,
            'file_hash' => $file_hash,
            'message' => $message,
        ]);
    }

    public static function PassportElementErrorSelfie(string $type,
        string $file_hash,
        string $message): self
    {
        return new PassportElementError(...[
            'source' => 'selfie',
            'type' => $type,
            'file_hash' => $file_hash,
            'message' => $message,
        ]);
    }

    public static function PassportElementErrorFile(string $type,
        string $file_hash,
        string $message): self
    {
        return new PassportElementError(...[
            'source' => 'file',
            'type' => $type,
            'file_hash' => $file_hash,
            'message' => $message,
        ]);
    }

    public static function PassportElementErrorFiles(string $type,
        array $file_hashes,
        string $message): self
    {
        return new PassportElementError(...[
            'source' => 'files',
            'type' => $type,
            'file_hashes' => $file_hashes,
            'message' => $message,
        ]);
    }

    public static function PassportElementErrorTranslationFile(string $type,
        string $file_hash,
        string $message): self
    {
        return new PassportElementError(...[
            'source' => 'translation_file',
            'type' => $type,
            'file_hash' => $file_hash,
            'message' => $message,
        ]);
    }

    public static function PassportElementErrorTranslationFiles(string $type,
        array $file_hashes,
        string $message): self
    {
        return new PassportElementError(...[
            'source' => 'translation_file',
            'type' => $type,
            'file_hashes' => $file_hashes,
            'message' => $message,
        ]);
    }

    public static function PassportElementErrorUnspecified(string $type,
        string $element_hash,
        string $message): self
    {
        return new PassportElementError(...[
            'source' => 'translation_file',
            'type' => $type,
            'element_hash' => $element_hash,
            'message' => $message,
        ]);
    }
}
