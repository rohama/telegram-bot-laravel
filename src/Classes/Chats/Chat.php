<?php

namespace Rohama\Telegram\Type\Chats;

use Rohama\Telegram\Type\TObj;

class Chat extends TObj
{
    public function __construct(public int $id,
        public string $type,
        public ?string $title = null,
        public ?string $username = null,
        public ?string $first_name = null,
        public ?string $last_name = null,
        public ?bool $is_forum = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
