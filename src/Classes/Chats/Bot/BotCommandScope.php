<?php

namespace Rohama\Telegram\Type\Chats\Bot;

use Rohama\Telegram\Type\TObj;

class BotCommandScope extends TObj
{
    public function __construct(public string $type,
        public string|int|null $chat_id = null,
        public ?int $user_id = null,
        ...$args)
    {
        parent::__construct(...$args);
    }

    public static function BotCommandScopeDefault(): self
    {
        return new BotCommandScope(...[
            'type' => 'default',
        ]);
    }

    public static function BotCommandScopeAllPrivateChats(): self
    {
        return new BotCommandScope(...[
            'type' => 'all_private_chats',
        ]);
    }

    public static function BotCommandScopeAllGroupChats(): self
    {
        return new BotCommandScope(...[
            'type' => 'all_group_chats',
        ]);
    }

    public static function BotCommandScopeAllChatAdministrators(): self
    {
        return new BotCommandScope(...[
            'type' => 'all_chat_administrators',
        ]);
    }

    public static function BotCommandScopeChat(string|int $chat_id): self
    {
        return new BotCommandScope(...[
            'type' => 'chat',
            'chat_id' => $chat_id,
        ]);
    }

    public static function BotCommandScopeChatAdministrators(string|int $chat_id): self
    {
        return new BotCommandScope(...[
            'type' => 'chat_administrators',
            'chat_id' => $chat_id,
        ]);
    }

    public static function BotCommandScopeChatMember(string|int $chat_id,
        int $user_id): self
    {
        return new BotCommandScope(...[
            'type' => 'chat_member',
            'chat_id' => $chat_id,
            'user_id' => $user_id,
        ]);
    }
}
