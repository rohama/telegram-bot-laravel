<?php

namespace Rohama\Telegram\Type\Chats\Bot;

use Rohama\Telegram\Type\TObj;

class BotDescription extends TObj
{
    public function __construct(public string $description,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
