<?php

namespace Rohama\Telegram\Type\Chats\Bot;

use Rohama\Telegram\Type\TObj;

class BotShortDescription extends TObj
{
    public function __construct(public string $short_description,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
