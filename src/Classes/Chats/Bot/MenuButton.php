<?php

namespace Rohama\Telegram\Type\Chats\Bot;

use Rohama\Telegram\Type\Messages\WebAppInfo;
use Rohama\Telegram\Type\TObj;

class MenuButton extends TObj
{
    public ?WebAppInfo $web_app;

    public function __construct(public string $type,
        public ?string $text = null,
        array|WebAppInfo|null $web_app = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->web_app = is_array($web_app) ? new WebAppInfo(...$web_app) : $web_app;
    }

    public static function MenuButtonCommands(): self
    {
        return new MenuButton(...[
            'type' => 'commands',
        ]);
    }

    public static function MenuButtonWebApp(string $text,
        array|WebAppInfo $web_app): self
    {
        return new MenuButton(...[
            'type' => 'web_app',
            'text' => $text,
            'web_app' => $web_app,
        ]);
    }

    public static function MenuButtonDefault(): self
    {
        return new MenuButton(...[
            'type' => 'default',
        ]);
    }
}
