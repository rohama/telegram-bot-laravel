<?php

namespace Rohama\Telegram\Type\Chats\Bot;

use Rohama\Telegram\Type\TObj;

class BotCommand extends TObj
{
    public function __construct(public string $command,
        public string $description,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
