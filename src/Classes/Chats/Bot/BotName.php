<?php

namespace Rohama\Telegram\Type\Chats\Bot;

use Rohama\Telegram\Type\TObj;

class BotName extends TObj
{
    public function __construct(public string $name,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
