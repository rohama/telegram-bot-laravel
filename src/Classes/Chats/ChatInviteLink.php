<?php

namespace Rohama\Telegram\Type\Chats;

use Rohama\Telegram\Type\TObj;

class ChatInviteLink extends TObj
{
    public User $creator;

    public function __construct(public string $invite_link,
        array|User $creator,
        public bool $creates_join_request,
        public bool $is_primary,
        public bool $is_revoked,
        public ?string $name = null,
        public ?int $expire_date = null,
        public ?int $member_limit = null,
        public ?int $pending_join_request_count = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->creator = is_array($creator) ? new User(...$creator) : $creator;
    }
}
