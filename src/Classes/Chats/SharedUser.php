<?php

namespace Rohama\Telegram\Type\Chats;

use Rohama\Telegram\Type\Messages\Media\PhotoSize;
use Rohama\Telegram\Type\TObj;

class SharedUser extends TObj
{
    public ?array $photo;

    public function __construct(public int $user_id,
        public ?string $first_name = null,
        public ?string $last_name = null,
        public ?string $username = null,
        ?array $photo = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->photo = is_null($photo) ? null : array_map(function ($photo_size) {
            return is_array($photo_size) ? new PhotoSize(...$photo_size) : $photo_size;
        }, $photo);
    }
}
