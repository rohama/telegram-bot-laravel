<?php

namespace Rohama\Telegram\Type\Chats;

use Rohama\Telegram\Type\Messages\Location;
use Rohama\Telegram\Type\TObj;

class ChatLocation extends TObj
{
    public Location $location;

    public function __construct(array|Location $location,
        public string $address,
        ...$args)
    {
        parent::__construct(...$args);
        $this->location = is_array($location) ? new Location(...$location) : $location;
    }
}
