<?php

namespace Rohama\Telegram\Type\Chats;

use Rohama\Telegram\Type\Chats\Business\BusinessIntro;
use Rohama\Telegram\Type\Chats\Business\BusinessLocation;
use Rohama\Telegram\Type\Chats\Business\BusinessOpeningHours;
use Rohama\Telegram\Type\Messages\ReactionType;
use Rohama\Telegram\Type\Update\Message;

class ChatFullInfo extends Chat
{
    public ?array $photo;

    public ?Birthdate $birthdate;

    public ?BusinessIntro $business_intro;

    public ?BusinessLocation $business_location;

    public ?BusinessOpeningHours $business_opening_hours;

    public ?Chat $personal_chat;

    public ?array $available_reactions;

    public ?Message $pinned_message;

    public ?ChatPermissions $permissions;

    public ?ChatLocation $location;

    public function __construct(public int $accent_color_id,
        public int $max_reaction_count,
        array|ChatPhoto|null $photo = null,
        public ?array $active_usernames = null,
        array|Birthdate|null $birthdate = null,
        array|BusinessIntro|null $business_intro = null,
        array|BusinessLocation|null $business_location = null,
        array|BusinessOpeningHours|null $business_opening_hours = null,
        array|Chat|null $personal_chat = null,
        ?array $available_reactions = null,
        public ?string $background_custom_emoji_id = null,
        public ?int $profile_accent_color_id = null,
        public ?string $profile_background_custom_emoji_id = null,
        public ?string $emoji_status_custom_emoji_id = null,
        public ?string $emoji_status_expiration_date = null,
        public ?string $bio = null,
        public ?bool $has_private_forwards = null,
        public ?bool $has_restricted_voice_and_video_messages = null,
        public ?bool $join_to_send_messages = null,
        public ?bool $join_by_request = null,
        public ?string $description = null,
        public ?string $invite_link = null,
        array|Message|null $pinned_message = null,
        array|ChatPermissions|null $permissions = null,
        public ?bool $can_send_paid_media = null,
        public ?int $slow_mode_delay = null,
        public ?int $unrestrict_boost_count = null,
        public ?int $message_auto_delete_time = null,
        public ?bool $has_aggressive_anti_spam_enabled = null,
        public ?bool $has_hidden_members = null,
        public ?bool $has_protected_content = null,
        public ?bool $has_visible_history = null,
        public ?string $sticker_set_name = null,
        public ?bool $can_set_sticker_set = null,
        public ?string $custom_emoji_sticker_set_name = null,
        public ?int $linked_chat_id = null,
        array|ChatLocation|null $location = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->birthdate = is_array($birthdate) ? new Birthdate(...$birthdate) : $birthdate;
        $this->business_intro = is_array($business_intro) ? new BusinessIntro(...$business_intro) : $business_intro;
        $this->business_location = is_array($business_location) ? new BusinessLocation(...$business_location) : $business_location;
        $this->business_opening_hours = is_array($business_opening_hours) ? new BusinessOpeningHours(...$business_opening_hours) : $business_opening_hours;
        $this->personal_chat = is_array($personal_chat) ? new Chat(...$personal_chat) : $personal_chat;
        $this->pinned_message = is_array($pinned_message) ? new Message(...$pinned_message) : $pinned_message;
        $this->permissions = is_array($permissions) ? new ChatPermissions(...$permissions) : $permissions;
        $this->location = is_array($location) ? new ChatLocation(...$location) : $location;
        $this->photo = is_array($photo) ? new ChatPhoto(...$photo) : $photo;
        $this->available_reactions = is_null($available_reactions) ? null : array_map(function ($available_reaction) {
            return is_array($available_reaction) ? new ReactionType(...$available_reaction) : $available_reaction;
        }, $available_reactions);
    }
}
