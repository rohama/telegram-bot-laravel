<?php

namespace Rohama\Telegram\Type\Chats\Business;

use Rohama\Telegram\Type\Sticker\Sticker;
use Rohama\Telegram\Type\TObj;

class BusinessIntro extends TObj
{
    public ?Sticker $sticker;

    public function __construct(public ?string $title = null,
        public ?string $message = null,
        array|Sticker|null $sticker = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->sticker = is_array($sticker) ? new Sticker(...$sticker) : $sticker;
    }
}
