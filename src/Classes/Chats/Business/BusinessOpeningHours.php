<?php

namespace Rohama\Telegram\Type\Chats\Business;

use Rohama\Telegram\Type\TObj;

class BusinessOpeningHours extends TObj
{
    public array $opening_hours;

    public function __construct(public string $time_zone_name,
        array $opening_hours,
        ...$args)
    {
        parent::__construct(...$args);
        $this->opening_hours = array_map(function ($opening_hour) {
            return is_array($opening_hour) ? new BusinessOpeningHoursInterval(...$opening_hour) : $opening_hour;
        }, $opening_hours);
    }
}
