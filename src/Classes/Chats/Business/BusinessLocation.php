<?php

namespace Rohama\Telegram\Type\Chats\Business;

use Rohama\Telegram\Type\Messages\Location;
use Rohama\Telegram\Type\TObj;

class BusinessLocation extends TObj
{
    public ?Location $location;

    public function __construct(public string $address,
        array|Location|null $location = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->location = is_array($location) ? new Location(...$location) : $location;
    }
}
