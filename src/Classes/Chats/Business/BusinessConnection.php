<?php

namespace Rohama\Telegram\Type\Chats\Business;

use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\TObj;

class BusinessConnection extends TObj
{
    public User $user;

    public function __construct(public string $id,
        array|User $user,
        public string $user_chat_id,
        public int $date,
        public bool $can_reply,
        public bool $is_enabled,
        ...$args)
    {
        parent::__construct(...$args);
        $this->user = is_array($user) ? new User(...$user) : $user;
    }
}
