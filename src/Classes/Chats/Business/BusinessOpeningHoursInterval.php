<?php

namespace Rohama\Telegram\Type\Chats\Business;

use Rohama\Telegram\Type\TObj;

class BusinessOpeningHoursInterval extends TObj
{
    public function __construct(public int $opening_minute,
        public int $closing_minute,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
