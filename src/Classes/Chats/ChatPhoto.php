<?php

namespace Rohama\Telegram\Type\Chats;

use Rohama\Telegram\Type\TObj;

class ChatPhoto extends TObj
{
    public function __construct(public string $small_file_id,
        public string $small_file_unique_id,
        public string $big_file_id,
        public string $big_file_unique_id,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
