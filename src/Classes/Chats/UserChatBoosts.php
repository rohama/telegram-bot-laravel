<?php

namespace Rohama\Telegram\Type\Chats;

use Rohama\Telegram\Type\TObj;

class UserChatBoosts extends TObj
{
    public array $boosts;

    public function __construct(array $boosts,
        ...$args)
    {
        parent::__construct(...$args);
        $this->boosts = array_map(function ($boost) {
            return is_array($boost) ? new ChatBoost(...$boost) : $boost;
        }, $boosts);
    }
}
