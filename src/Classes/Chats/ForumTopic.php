<?php

namespace Rohama\Telegram\Type\Chats;

use Rohama\Telegram\Type\TObj;

class ForumTopic extends TObj
{
    public function __construct(public int $message_thread_id,
        public string $name,
        public int $icon_color,
        public ?string $icon_custom_emoji_id = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
