<?php

namespace Rohama\Telegram\Type\Chats;

use Rohama\Telegram\Type\TObj;

class UsersShared extends TObj
{
    public array $users;

    public function __construct(public int $request_id,
        array $users,
        ...$args)
    {
        parent::__construct(...$args);
        $this->users = array_map(function ($user) {
            return is_array($user) ? new SharedUser(...$user) : $user;
        }, $users);
    }
}
