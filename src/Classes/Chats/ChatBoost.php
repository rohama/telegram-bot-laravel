<?php

namespace Rohama\Telegram\Type\Chats;

use Rohama\Telegram\Type\TObj;

class ChatBoost extends TObj
{
    public ChatBoostSource $source;

    public function __construct(public string $boost_id,
        public int $add_date,
        public int $expiration_date,
        array|ChatBoostSource $source,
        ...$args)
    {
        parent::__construct(...$args);
        $this->source = is_array($source) ? new ChatBoostSource(...$source) : $source;
    }
}
