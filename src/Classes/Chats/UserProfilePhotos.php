<?php

namespace Rohama\Telegram\Type\Chats;

use Rohama\Telegram\Type\Messages\Media\PhotoSize;
use Rohama\Telegram\Type\TObj;

class UserProfilePhotos extends TObj
{
    public array $photos;

    public function __construct(public int $total_count,
        array $photos,
        ...$args)
    {
        parent::__construct(...$args);
        $this->photos = array_map(function ($photo) {
            return array_map(function ($photo_size) {
                return is_array($photo_size) ? new PhotoSize(...$photo_size) : $photo_size;
            }, $photo);
        }, $photos);
    }
}
