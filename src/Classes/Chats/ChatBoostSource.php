<?php

namespace Rohama\Telegram\Type\Chats;

use Rohama\Telegram\Type\TObj;

class ChatBoostSource extends TObj
{
    public ?User $user;

    public function __construct(public string $source,
        public ?int $giveaway_message_id = null,
        public ?bool $is_unclaimed = null,
        array|User|null $user = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->user = is_array($user) ? new User(...$user) : $user;
    }
}
