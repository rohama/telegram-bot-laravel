<?php

namespace Rohama\Telegram\Type\Chats;

use Rohama\Telegram\Type\TObj;

class ChatMember extends TObj
{
    public User $user;

    public function __construct(public string $status,
        array|User $user,
        public ?bool $is_anonymous = null,
        public ?string $custom_title = null,
        public ?bool $can_be_edited = null,
        public ?int $until_date = null,
        public ?bool $can_manage_chat = null,
        public ?bool $can_delete_messages = null,
        public ?bool $can_manage_video_chats = null,
        public ?bool $can_restrict_members = null,
        public ?bool $can_promote_members = null,
        public ?bool $can_change_info = null,
        public ?bool $can_invite_users = null,
        public ?bool $can_post_stories = null,
        public ?bool $can_edit_stories = null,
        public ?bool $can_delete_stories = null,
        public ?bool $can_post_messages = null,
        public ?bool $can_edit_messages = null,
        public ?bool $can_pin_messages = null,
        public ?bool $can_manage_topics = null,
        public ?bool $can_send_messages = null,
        public ?bool $can_send_audios = null,
        public ?bool $can_send_documents = null,
        public ?bool $can_send_photos = null,
        public ?bool $can_send_videos = null,
        public ?bool $can_send_video_notes = null,
        public ?bool $can_send_voice_notes = null,
        public ?bool $can_send_polls = null,
        public ?bool $can_send_other_messages = null,
        public ?bool $can_add_web_page_previews = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->user = is_array($user) ? new User(...$user) : $user;
    }
}
