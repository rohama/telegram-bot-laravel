<?php

namespace Rohama\Telegram\Type\Chats;

use Rohama\Telegram\Type\TObj;

class Birthdate extends TObj
{
    public function __construct(public int $day,
        public int $month,
        public int $year,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
