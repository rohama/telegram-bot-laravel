<?php

namespace Rohama\Telegram\Type\Chats\Background;

use Rohama\Telegram\Type\Messages\Media\Document;
use Rohama\Telegram\Type\TObj;

class BackgroundType extends TObj
{
    public ?BackgroundFill $fill;

    public ?Document $document;

    public function __construct(public string $type,
        array|BackgroundFill|null $fill = null,
        public ?int $dark_theme_dimming = null,
        array|Document|null $document = null,
        public ?bool $is_blurred = null,
        public ?bool $is_moving = null,
        public ?int $intensity = null,
        public ?bool $is_inverted = null,
        public ?string $theme_name = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->fill = is_array($fill) ? new BackgroundFill(...$fill) : $fill;
        $this->document = is_array($document) ? new Document(...$document) : $document;
    }
}
