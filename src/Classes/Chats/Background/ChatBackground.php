<?php

namespace Rohama\Telegram\Type\Chats\Background;

use Rohama\Telegram\Type\TObj;

class ChatBackground extends TObj
{
    public BackgroundType $type;

    public function __construct(array|BackgroundType $type,
        ...$args)
    {
        parent::__construct(...$args);
        $this->type = is_array($type) ? new BackgroundType(...$type) : $type;
    }
}
