<?php

namespace Rohama\Telegram\Type\Chats\Background;

use Rohama\Telegram\Type\TObj;

class BackgroundFill extends TObj
{
    public function __construct(public string $type,
        public ?int $color = null,
        public ?int $top_color = null,
        public ?int $bottom_color = null,
        public ?int $rotation_angle = null,
        public ?array $colors = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
