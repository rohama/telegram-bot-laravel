<?php

namespace Rohama\Telegram\Type\Payment;

use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\TObj;

class PreCheckoutQuery extends TObj
{
    public User $from;

    public ?OrderInfo $order_info;

    public function __construct(public string $id,
        array|User $from,
        public string $currency,
        public int $total_amount,
        public string $invoice_payload,
        public ?string $shipping_option_id = null,
        array|OrderInfo|null $order_info = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->from = is_array($from) ? new User(...$from) : $from;
        $this->order_info = is_array($order_info) ? new OrderInfo(...$order_info) : $order_info;
    }
}
