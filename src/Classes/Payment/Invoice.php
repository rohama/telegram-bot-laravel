<?php

namespace Rohama\Telegram\Type\Payment;

use Rohama\Telegram\Type\TObj;

class Invoice extends TObj
{
    public function __construct(public string $title,
        public string $description,
        public string $start_parameter,
        public string $currency,
        public int $total_amount,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
