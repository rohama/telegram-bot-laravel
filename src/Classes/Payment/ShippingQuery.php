<?php

namespace Rohama\Telegram\Type\Payment;

use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\TObj;

class ShippingQuery extends TObj
{
    public User $from;

    public ShippingAddress $shipping_address;

    public function __construct(public string $id,
        array|User $from,
        public string $invoice_payload,
        array|ShippingAddress $shipping_address,
        ...$args)
    {
        parent::__construct(...$args);
        $this->from = is_array($from) ? new User(...$from) : $from;
        $this->shipping_address = is_array($shipping_address) ? new ShippingAddress(...$shipping_address) : $shipping_address;
    }
}
