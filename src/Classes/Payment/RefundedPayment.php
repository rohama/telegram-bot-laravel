<?php

namespace Rohama\Telegram\Type\Payment;

use Rohama\Telegram\Type\TObj;

class RefundedPayment extends TObj
{
    public function __construct(public string $currency,
        public int $total_amount,
        public string $invoice_payload,
        public string $telegram_payment_charge_id,
        public string $provider_payment_charge_id,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
