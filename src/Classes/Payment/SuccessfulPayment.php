<?php

namespace Rohama\Telegram\Type\Payment;

use Rohama\Telegram\Type\TObj;

class SuccessfulPayment extends TObj
{
    public ?OrderInfo $order_info;

    public function __construct(public string $currency,
        public int $total_amount,
        public string $invoice_payload,
        public string $telegram_payment_charge_id,
        public string $provider_payment_charge_id,
        array|OrderInfo|null $order_info = null,
        public ?string $shipping_option_id = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->order_info = is_array($order_info) ? new OrderInfo(...$order_info) : $order_info;
    }
}
