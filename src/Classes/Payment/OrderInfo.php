<?php

namespace Rohama\Telegram\Type\Payment;

use Rohama\Telegram\Type\TObj;

class OrderInfo extends TObj
{
    public ?ShippingAddress $shipping_address;

    public function __construct(public ?string $name = null,
        public ?string $phone_number = null,
        public ?string $email = null,
        array|ShippingAddress|null $shipping_address = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->shipping_address = is_array($shipping_address) ? new ShippingAddress(...$shipping_address) : $shipping_address;
    }
}
