<?php

namespace Rohama\Telegram\Type\Payment;

use Rohama\Telegram\Type\TObj;

class ShippingAddress extends TObj
{
    public function __construct(public string $country_code,
        public string $state,
        public string $city,
        public string $street_line1,
        public string $street_line2,
        public string $post_code,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
