<?php

namespace Rohama\Telegram\Type\Payment;

use Rohama\Telegram\Type\TObj;

class ShippingOption extends TObj
{
    public array $prices;

    public function __construct(public string $id,
        public string $title,
        array $prices,
        ...$args)
    {
        parent::__construct(...$args);
        $this->prices = array_map(function ($price) {
            return is_array($price) ? new LabeledPrice(...$price) : $price;
        }, $prices);
    }
}
