<?php

namespace Rohama\Telegram\Type\Payment;

use Rohama\Telegram\Type\TObj;

class RevenueWithdrawalState extends TObj
{
    public function __construct(public string $type,
        public ?int $date = null,
        public ?string $url = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
