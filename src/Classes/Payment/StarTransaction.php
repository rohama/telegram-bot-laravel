<?php

namespace Rohama\Telegram\Type\Payment;

use Rohama\Telegram\Type\TObj;

class StarTransaction extends TObj
{
    public ?TransactionPartner $source;

    public ?TransactionPartner $receiver;

    public function __construct(public string $id,
        public int $amount,
        public int $date,
        array|TransactionPartner|null $source = null,
        array|TransactionPartner|null $receiver = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->source = is_array($source) ? new TransactionPartner(...$source) : $source;
        $this->receiver = is_array($receiver) ? new TransactionPartner(...$receiver) : $receiver;
    }
}
