<?php

namespace Rohama\Telegram\Type\Payment;

use Rohama\Telegram\Type\TObj;

class StarTransactions extends TObj
{
    public array $transactions;

    public function __construct(array $transactions,
        ...$args)
    {
        parent::__construct(...$args);
        $this->transactions = array_map(function ($transaction) {
            return is_array($transaction) ? new StarTransaction(...$transaction) : $transaction;
        }, $transactions);
    }
}
