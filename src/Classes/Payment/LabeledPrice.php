<?php

namespace Rohama\Telegram\Type\Payment;

use Rohama\Telegram\Type\TObj;

class LabeledPrice extends TObj
{
    public function __construct(public string $label,
        public int $amount,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
