<?php

namespace Rohama\Telegram\Type\Payment;

use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\Messages\Media\PaidMedia;
use Rohama\Telegram\Type\TObj;

class TransactionPartner extends TObj
{
    public ?User $user;

    public ?array $paid_media;

    public ?RevenueWithdrawalState $withdrawal_state;

    public function __construct(public string $type,
        public ?string $invoice_payload = null,
        ?array $paid_media = null,
        array|OrderInfo|null $user = null,
        array|RevenueWithdrawalState|null $withdrawal_state = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->user = is_array($user) ? new User(...$user) : $user;
        $this->paid_media = is_null($paid_media) ? null : array_map(function ($media) {
            return is_array($media) ? new PaidMedia(...$media) : $media;
        }, $paid_media);
        $this->withdrawal_state = is_array($withdrawal_state) ? new RevenueWithdrawalState(...$withdrawal_state) : $withdrawal_state;
    }
}
