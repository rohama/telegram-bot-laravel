<?php

namespace Rohama\Telegram\Type;

class InputFile
{
    public function __construct(private string $file_path,
        private ?string $name = null)
    {
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function toMultipart(): bool|array
    {
        if (isset($this->name)) {
            return [
                'name' => $this->name,
                'contents' => fopen($this->file_path, 'r'),
            ];
        } else {
            return false;
        }
    }

    public function toString(): string
    {
        return 'attach://'.$this->name;
    }

    public function __toString()
    {
        return $this->toString();
    }
}
