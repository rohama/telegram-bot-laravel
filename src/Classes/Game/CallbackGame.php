<?php

namespace Rohama\Telegram\Type\Game;

use Rohama\Telegram\Type\TObj;

class CallbackGame extends TObj
{
    public function __construct(...$args)
    {
        parent::__construct(...$args);
    }
}
