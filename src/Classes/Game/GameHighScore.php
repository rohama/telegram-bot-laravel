<?php

namespace Rohama\Telegram\Type\Game;

use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\TObj;

class GameHighScore extends TObj
{
    public ?User $user;

    public function __construct(public int $position,
        public int $score,
        array|User $user,
        ...$args)
    {
        parent::__construct(...$args);
        $this->user = is_array($user) ? new User(...$user) : $user;
    }
}
