<?php

namespace Rohama\Telegram\Type\Game;

use Rohama\Telegram\Type\Messages\Media\Animation;
use Rohama\Telegram\Type\Messages\Media\PhotoSize;
use Rohama\Telegram\Type\Messages\MessageEntity;
use Rohama\Telegram\Type\TObj;

class Game extends TObj
{
    public array $photo;

    public ?array $text_entities;

    public ?Animation $animation;

    public function __construct(public string $title,
        public string $description,
        array $photo,
        public ?string $text = null,
        ?array $text_entities = null,
        array|Animation|null $animation = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->photo = array_map(function ($photo_size) {
            return is_array($photo_size) ? new PhotoSize(...$photo_size) : $photo_size;
        }, $photo);
        $this->text_entities = is_null($text_entities) ? null : array_map(function ($entity) {
            return is_array($entity) ? new MessageEntity(...$entity) : $entity;
        }, $text_entities);
        $this->animation = is_array($animation) ? new Animation(...$animation) : $animation;
    }
}
