<?php

namespace Rohama\Telegram\Type\Messages;

use Rohama\Telegram\Type\TObj;

class WebAppData extends TObj
{
    public function __construct(public string $data,
        public string $button_text,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
