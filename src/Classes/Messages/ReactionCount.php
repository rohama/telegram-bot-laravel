<?php

namespace Rohama\Telegram\Type\Messages;

use Rohama\Telegram\Type\TObj;

class ReactionCount extends TObj
{
    public ReactionType $type;

    public function __construct(array|ReactionType $type,
        public int $total_count,
        ...$args)
    {
        parent::__construct(...$args);
        $this->type = is_array($type) ? new ReactionType(...$type) : $type;
    }
}
