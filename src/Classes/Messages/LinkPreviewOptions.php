<?php

namespace Rohama\Telegram\Type\Messages;

use Rohama\Telegram\Type\TObj;

class LinkPreviewOptions extends TObj
{
    public function __construct(public ?bool $is_disabled = null,
        public ?string $url = null,
        public ?bool $prefer_small_media = null,
        public ?bool $prefer_large_media = null,
        public ?bool $show_above_text = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
