<?php

namespace Rohama\Telegram\Type\Messages\Keyboard;

use Rohama\Telegram\Type\TObj;

class InlineKeyboardMarkup extends TObj
{
    public array $inline_keyboard;

    public function __construct(array $inline_keyboard,
        ...$args)
    {
        parent::__construct(...$args);
        $this->inline_keyboard = array_map(function ($row) {
            return array_map(function ($button) {
                return is_array($button) ? new InlineKeyboardButton(...$button) : $button;
            }, $row);
        }, $inline_keyboard);
    }

    /**
     * add a row of buttons to keyboard
     *
     * @param  array  $buttons  the row of buttons
     */
    public function addRow(array $buttons): self
    {
        $this->inline_keyboard[] = array_map(function ($button) {
            return is_array($button) ? new InlineKeyboardButton(...$button) : $button;
        }, $buttons);

        return $this;
    }

    /**
     * set the specific row of keyboard buttons
     *
     * @param  int  $row  the number of the row
     * @param  array  $buttons  the row of buttons
     */
    public function setRow(int $row, array $buttons): self
    {
        $this->inline_keyboard[$row] = array_map(function ($button) {
            return is_array($button) ? new InlineKeyboardButton(...$button) : $button;
        }, $buttons);
        $this->inline_keyboard = array_values($this->inline_keyboard);

        return $this;
    }

    /**
     * add a button to the end of specific row
     *
     * @param  int  $row  the row number
     * @param  array|InlineKeyboardButton  $button  the keyboard button to add
     */
    public function addButtonToRow(int $row, array|InlineKeyboardButton $button): self
    {
        $this->inline_keyboard[$row][] = is_array($button) ? new InlineKeyboardButton(...$button) : $button;
        $this->inline_keyboard = array_values($this->inline_keyboard);

        return $this;
    }
}
