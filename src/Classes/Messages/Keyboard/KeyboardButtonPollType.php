<?php

namespace Rohama\Telegram\Type\Messages\Keyboard;

use Rohama\Telegram\Type\TObj;

class KeyboardButtonPollType extends TObj
{
    public function __construct(public ?string $type = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
