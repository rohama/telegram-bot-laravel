<?php

namespace Rohama\Telegram\Type\Messages\Keyboard;

use Rohama\Telegram\Type\TObj;

class KeyboardButtonRequestUsers extends TObj
{
    public function __construct(public int $request_id,
        public ?bool $user_is_bot = null,
        public ?bool $user_is_premium = null,
        public ?int $max_quantity = null,
        public ?bool $request_name = null,
        public ?bool $request_username = null,
        public ?bool $request_photo = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
