<?php

namespace Rohama\Telegram\Type\Messages\Keyboard;

use Rohama\Telegram\Type\TObj;

class ReplyKeyboardMarkup extends TObj
{
    public array $keyboard;

    public function __construct(array $keyboard,
        public ?bool $is_persistent = null,
        public ?bool $resize_keyboard = null,
        public ?bool $one_time_keyboard = null,
        public ?string $input_field_placeholder = null,
        public ?bool $selective = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->keyboard = array_map(function ($row) {
            return array_map(function ($button) {
                return is_array($button) ? new KeyboardButton(...$button) : $button;
            }, $row);
        }, $keyboard);
    }

    /**
     * add a row of buttons to keyboard
     *
     * @param  array  $buttons  the row of buttons
     */
    public function addRow(array $buttons): self
    {
        $this->keyboard[] = array_map(function ($button) {
            return is_array($button) ? new KeyboardButton(...$button) : $button;
        }, $buttons);

        return $this;
    }

    /**
     * set the specific row of keyboard buttons
     *
     * @param  int  $row  the number of the row
     * @param  array  $buttons  the row of buttons
     */
    public function setRow(int $row, array $buttons): self
    {
        $this->keyboard[$row] = array_map(function ($button) {
            return is_array($button) ? new KeyboardButton(...$button) : $button;
        }, $buttons);
        $this->keyboard = array_values($this->keyboard);

        return $this;
    }

    /**
     * add a button to the end of specific row
     *
     * @param  int  $row  the row number
     * @param  array|KeyboardButton  $button  the keyboard button to add
     */
    public function addButtonToRow(int $row, array|KeyboardButton $button): self
    {
        $this->keyboard[$row][] = is_array($button) ? new KeyboardButton(...$button) : $button;
        $this->keyboard = array_values($this->keyboard);

        return $this;
    }
}
