<?php

namespace Rohama\Telegram\Type\Messages\Keyboard;

use Rohama\Telegram\Type\Game\CallbackGame;
use Rohama\Telegram\Type\Messages\WebAppInfo;
use Rohama\Telegram\Type\TObj;

class InlineKeyboardButton extends TObj
{
    public ?WebAppInfo $web_app;

    public ?LoginUrl $login_url;

    public ?SwitchInlineQueryChosenChat $switch_inline_query_chosen_chat;

    public ?CallbackGame $callback_game;

    public function __construct(public string $text,
        public ?string $url = null,
        public ?string $callback_data = null,
        array|WebAppInfo|null $web_app = null,
        array|LoginUrl|null $login_url = null,
        public ?string $switch_inline_query = null,
        public ?string $switch_inline_query_current_chat = null,
        array|SwitchInlineQueryChosenChat|null $switch_inline_query_chosen_chat = null,
        array|CallbackGame|null $callback_game = null,
        public ?bool $pay = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->web_app = is_array($web_app) ? new WebAppInfo(...$web_app) : $web_app;
        $this->login_url = is_array($login_url) ? new LoginUrl(...$login_url) : $login_url;
        $this->switch_inline_query_chosen_chat = is_array($switch_inline_query_chosen_chat) ? new SwitchInlineQueryChosenChat(...$switch_inline_query_chosen_chat) : $switch_inline_query_chosen_chat;
        $this->callback_game = is_array($callback_game) ? new CallbackGame(...$callback_game) : $callback_game;
    }
}
