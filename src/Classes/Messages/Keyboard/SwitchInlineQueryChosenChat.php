<?php

namespace Rohama\Telegram\Type\Messages\Keyboard;

use Rohama\Telegram\Type\TObj;

class SwitchInlineQueryChosenChat extends TObj
{
    public function __construct(public string $query,
        public ?bool $allow_user_chats = null,
        public ?bool $allow_bot_chats = null,
        public ?bool $allow_group_chats = null,
        public ?bool $allow_channel_chats = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
