<?php

namespace Rohama\Telegram\Type\Messages\Keyboard;

use Rohama\Telegram\Type\Chats\ChatAdministratorRights;
use Rohama\Telegram\Type\TObj;

class KeyboardButtonRequestChat extends TObj
{
    public ?ChatAdministratorRights $user_administrator_rights;

    public ?ChatAdministratorRights $bot_administrator_rights;

    public function __construct(public int $request_id,
        public bool $chat_is_channel,
        public ?bool $chat_is_forum = null,
        array|ChatAdministratorRights|null $user_administrator_rights = null,
        array|ChatAdministratorRights|null $bot_administrator_rights = null,
        public ?bool $chat_has_username = null,
        public ?bool $chat_is_created = null,
        public ?bool $bot_is_member = null,
        public ?bool $request_title = null,
        public ?bool $request_username = null,
        public ?bool $request_photo = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->user_administrator_rights = is_array($user_administrator_rights) ? new ChatAdministratorRights(...$user_administrator_rights) : $user_administrator_rights;
        $this->bot_administrator_rights = is_array($bot_administrator_rights) ? new ChatAdministratorRights(...$bot_administrator_rights) : $bot_administrator_rights;
    }
}
