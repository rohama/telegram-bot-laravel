<?php

namespace Rohama\Telegram\Type\Messages\Keyboard;

use Rohama\Telegram\Type\TObj;

class ReplyKeyboardRemove extends TObj
{
    public bool $remove_keyboard = true;

    public function __construct(public ?bool $selective = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
