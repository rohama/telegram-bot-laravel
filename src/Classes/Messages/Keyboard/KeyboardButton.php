<?php

namespace Rohama\Telegram\Type\Messages\Keyboard;

use Rohama\Telegram\Type\Messages\WebAppInfo;
use Rohama\Telegram\Type\TObj;

class KeyboardButton extends TObj
{
    public ?KeyboardButtonRequestUsers $request_users;

    public ?KeyboardButtonRequestChat $request_chat;

    public ?KeyboardButtonPollType $request_poll;

    public ?WebAppInfo $web_app;

    public function __construct(public string $text,
        array|KeyboardButtonRequestUsers|null $request_users = null,
        array|KeyboardButtonRequestChat|null $request_chat = null,
        public ?bool $request_contact = null,
        public ?bool $request_location = null,
        array|KeyboardButtonPollType|null $request_poll = null,
        array|WebAppInfo|null $web_app = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->request_users = is_array($request_users) ? new KeyboardButtonRequestUsers(...$request_users) : $request_users;
        $this->request_chat = is_array($request_chat) ? new KeyboardButtonRequestChat(...$request_chat) : $request_chat;
        $this->request_poll = is_array($request_poll) ? new KeyboardButtonPollType(...$request_poll) : $request_poll;
        $this->web_app = is_array($web_app) ? new WebAppInfo(...$web_app) : $web_app;
    }
}
