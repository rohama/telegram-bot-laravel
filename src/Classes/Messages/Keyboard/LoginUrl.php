<?php

namespace Rohama\Telegram\Type\Messages\Keyboard;

use Rohama\Telegram\Type\TObj;

class LoginUrl extends TObj
{
    public function __construct(public string $url,
        public ?string $forward_text = null,
        public ?string $bot_username = null,
        public ?bool $request_write_access = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
