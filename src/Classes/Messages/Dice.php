<?php

namespace Rohama\Telegram\Type\Messages;

use Rohama\Telegram\Type\TObj;

class Dice extends TObj
{
    public function __construct(public string $emoji,
        public int $value,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
