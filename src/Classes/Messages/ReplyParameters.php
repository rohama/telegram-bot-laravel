<?php

namespace Rohama\Telegram\Type\Messages;

use Rohama\Telegram\Type\TObj;

class ReplyParameters extends TObj
{
    public ?array $quote_entities;

    public function __construct(public int $message_id,
        public int|string|null $chat_id = null,
        public ?bool $allow_sending_without_reply = null,
        public ?string $quote = null,
        public ?string $quote_parse_mode = null,
        public ?int $quote_position = null,
        ?array $quote_entities = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->quote_entities = is_null($quote_entities) ? null : array_map(function ($entity) {
            return is_array($entity) ? new MessageEntity(...$entity) : $entity;
        }, $quote_entities);
    }
}
