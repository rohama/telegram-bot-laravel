<?php

namespace Rohama\Telegram\Type\Messages;

use Rohama\Telegram\Type\TObj;

class WebAppInfo extends TObj
{
    public function __construct(public string $url,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
