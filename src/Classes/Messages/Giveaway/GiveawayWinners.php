<?php

namespace Rohama\Telegram\Type\Messages\Giveaway;

use Rohama\Telegram\Type\Chats\Chat;
use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\TObj;

class GiveawayWinners extends TObj
{
    public Chat $chat;

    public array $winners;

    public function __construct(array|Chat $chat,
        public int $giveaway_message_id,
        public int $winners_selection_date,
        public int $winner_count,
        array $winners,
        public ?int $additional_chat_count = null,
        public ?int $premium_subscription_month_count = null,
        public ?int $unclaimed_prize_count = null,
        public ?bool $only_new_members = null,
        public ?bool $was_refunded = null,
        public ?string $prize_description = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->chat = is_array($chat) ? new Chat(...$chat) : $chat;
        $this->winners = array_map(function ($winner) {
            return is_array($winner) ? new User(...$winner) : $winner;
        }, $winners);
    }
}
