<?php

namespace Rohama\Telegram\Type\Messages\Giveaway;

use Rohama\Telegram\Type\Chats\Chat;
use Rohama\Telegram\Type\TObj;

class Giveaway extends TObj
{
    public array $chats;

    public function __construct(array $chats,
        public int $winners_selection_date,
        public int $winner_count,
        public ?bool $only_new_members = null,
        public ?bool $has_public_winners = null,
        public ?string $prize_description = null,
        public ?array $country_codes = null,
        public ?int $premium_subscription_month_count = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->chats = array_map(function ($chat) {
            return is_array($chat) ? new Chat(...$chat) : $chat;
        }, $chats);
    }
}
