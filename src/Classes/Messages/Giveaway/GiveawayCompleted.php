<?php

namespace Rohama\Telegram\Type\Messages\Giveaway;

use Rohama\Telegram\Type\TObj;
use Rohama\Telegram\Type\Update\Message;

class GiveawayCompleted extends TObj
{
    public ?Message $giveaway_message;

    public function __construct(public int $winner_count,
        public ?int $unclaimed_prize_count = null,
        array|Message|null $giveaway_message = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->giveaway_message = is_array($giveaway_message) ? new Message(...$giveaway_message) : $giveaway_message;
    }
}
