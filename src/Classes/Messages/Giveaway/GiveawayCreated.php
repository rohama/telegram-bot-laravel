<?php

namespace Rohama\Telegram\Type\Messages\Giveaway;

use Rohama\Telegram\Type\TObj;

class GiveawayCreated extends TObj
{
    public function __construct(...$args)
    {
        parent::__construct(...$args);
    }
}
