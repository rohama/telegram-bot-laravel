<?php

namespace Rohama\Telegram\Type\Messages;

use Rohama\Telegram\Type\TObj;

class Venue extends TObj
{
    public Location $location;

    public function __construct(array|Location $location,
        public string $title,
        public string $address,
        public ?string $foursquare_id = null,
        public ?string $foursquare_type = null,
        public ?string $google_place_id = null,
        public ?string $google_place_type = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->location = is_array($location) ? new Location(...$location) : $location;
    }
}
