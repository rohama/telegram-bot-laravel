<?php

namespace Rohama\Telegram\Type\Messages;

use Rohama\Telegram\Type\TObj;

class TextQuote extends TObj
{
    public ?array $entities;

    public function __construct(public string $text,
        public int $position,
        public ?bool $is_manual = null,
        ?array $entities = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->entities = is_null($entities) ? null : array_map(function ($entity) {
            return is_array($entity) ? new MessageEntity(...$entity) : $entity;
        }, $entities);
    }
}
