<?php

namespace Rohama\Telegram\Type\Messages;

use Rohama\Telegram\Type\TObj;

class MessageId extends TObj
{
    public function __construct(public int $message_id,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
