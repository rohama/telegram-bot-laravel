<?php

namespace Rohama\Telegram\Type\Messages;

use Rohama\Telegram\Type\Chats\Chat;
use Rohama\Telegram\Type\TObj;

class InaccessibleMessage extends TObj
{
    public int $date = 0;

    public function __construct(array|Chat $chat,
        public int $message_id,
        ...$args)
    {
        $this->chat = is_array($chat) ? new Chat(...$chat) : $chat;
        parent::__construct(...$args);
    }
}
