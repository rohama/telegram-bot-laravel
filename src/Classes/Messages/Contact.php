<?php

namespace Rohama\Telegram\Type\Messages;

use Rohama\Telegram\Type\TObj;

class Contact extends TObj
{
    public function __construct(public string $phone_number,
        public string $first_name,
        public ?string $last_name = null,
        public ?int $user_id = null,
        public ?string $vcard = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
