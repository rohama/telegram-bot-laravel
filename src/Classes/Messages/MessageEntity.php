<?php

namespace Rohama\Telegram\Type\Messages;

use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\TObj;

class MessageEntity extends TObj
{
    public ?User $user;

    public function __construct(public string $type,
        public int $offset,
        public int $length,
        array|User|null $user = null,
        public ?string $url = null,
        public ?string $language = null,
        public ?string $custom_emoji_id = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->user = is_array($user) ? new User(...$user) : $user;
    }
}
