<?php

namespace Rohama\Telegram\Type\Messages;

use Rohama\Telegram\Type\Chats\Chat;
use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\TObj;

class MessageOrigin extends TObj
{
    public ?User $sender_user;

    public ?Chat $sender_chat;

    public ?Chat $chat;

    public function __construct(public string $type,
        public int $date,
        public int $length,
        array|User|null $sender_user = null,
        public ?string $sender_user_name = null,
        array|Chat|null $sender_chat = null,
        public ?string $author_signature = null,
        array|Chat|null $chat = null,
        public ?string $message_id = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->sender_user = is_array($sender_user) ? new User(...$sender_user) : $sender_user;
        $this->sender_chat = is_array($sender_chat) ? new Chat(...$sender_chat) : $sender_chat;
        $this->chat = is_array($chat) ? new Chat(...$chat) : $chat;
    }
}
