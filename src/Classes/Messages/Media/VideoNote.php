<?php

namespace Rohama\Telegram\Type\Messages\Media;

class VideoNote extends Media
{
    public ?PhotoSize $thumbnail;

    public function __construct(public int $length,
        public int $duration,
        array|PhotoSize|null $thumbnail = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->thumbnail = is_array($thumbnail) ? new PhotoSize(...$thumbnail) : $thumbnail;
    }
}
