<?php

namespace Rohama\Telegram\Type\Messages\Media;

use Rohama\Telegram\Type\Chats\Chat;
use Rohama\Telegram\Type\TObj;

class Story extends TObj
{
    public ?Chat $chat;

    public function __construct(public int $id,
        array|Chat|null $chat,
        ...$args)
    {
        parent::__construct(...$args);
        $this->chat = is_array($chat) ? new Chat(...$chat) : $chat;
    }
}
