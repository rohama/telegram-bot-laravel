<?php

namespace Rohama\Telegram\Type\Messages\Media;

class Audio extends Media
{
    public ?PhotoSize $thumbnail;

    public function __construct(public int $duration,
        public ?string $performer = null,
        public ?string $title = null,
        array|PhotoSize|null $thumbnail = null,
        public ?string $file_name = null,
        public ?string $mime_type = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->thumbnail = is_array($thumbnail) ? new PhotoSize(...$thumbnail) : $thumbnail;
    }
}
