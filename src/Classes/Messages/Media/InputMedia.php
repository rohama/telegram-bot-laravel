<?php

namespace Rohama\Telegram\Type\Messages\Media;

use Rohama\Telegram\Type\InputFile;
use Rohama\Telegram\Type\Messages\MessageEntity;
use Rohama\Telegram\Type\TObj;

class InputMedia extends TObj
{
    public ?array $caption_entities;

    public function __construct(public string $type,
        public string $media,
        public ?string $caption = null,
        public ?string $parse_mode = null,
        ?array $caption_entities = null,
        public string|InputFile|null $thumbnail = null,
        public ?bool $show_caption_above_media = null,
        public ?bool $has_spoiler = null,
        public ?int $width = null,
        public ?int $height = null,
        public ?int $duration = null,
        public ?bool $supports_streaming = null,
        public ?string $performer = null,
        public ?string $title = null,
        public ?bool $disable_content_type_detection = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->caption_entities = is_null($caption_entities) ? null : array_map(function ($entity) {
            return is_array($entity) ? new MessageEntity(...$entity) : $entity;
        }, $caption_entities);
    }

    public static function InputMediaPhoto(string $media,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        ?bool $show_caption_above_media = null,
        ?bool $has_spoiler = null): self
    {
        return new InputMedia(...[
            'type' => 'photo',
            'media' => $media,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'show_caption_above_media' => $show_caption_above_media,
            'has_spoiler' => $has_spoiler,
        ]);
    }

    public static function InputMediaVideo(string $media,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        string|InputFile|null $thumbnail = null,
        ?bool $show_caption_above_media = null,
        ?int $width = null,
        ?int $height = null,
        ?int $duration = null,
        ?bool $supports_streaming = null,
        ?bool $has_spoiler = null): self
    {
        return new InputMedia(...[
            'type' => 'video',
            'media' => $media,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'thumbnail' => $thumbnail,
            'show_caption_above_media' => $show_caption_above_media,
            'width' => $width,
            'height' => $height,
            'duration' => $duration,
            'supports_streaming' => $supports_streaming,
            'has_spoiler' => $has_spoiler,
        ]);
    }

    public static function InputMediaAnimation(string $media,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        string|InputFile|null $thumbnail = null,
        ?bool $show_caption_above_media = null,
        ?int $width = null,
        ?int $height = null,
        ?int $duration = null,
        ?bool $has_spoiler = null): self
    {
        return new InputMedia(...[
            'type' => 'animation',
            'media' => $media,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'thumbnail' => $thumbnail,
            'show_caption_above_media' => $show_caption_above_media,
            'width' => $width,
            'height' => $height,
            'duration' => $duration,
            'has_spoiler' => $has_spoiler,
        ]);
    }

    public static function InputMediaAudio(string $media,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        string|InputFile|null $thumbnail = null,
        ?int $duration = null,
        ?string $performer = null,
        ?string $title = null): self
    {
        return new InputMedia(...[
            'type' => 'audio',
            'media' => $media,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'thumbnail' => $thumbnail,
            'duration' => $duration,
            'performer' => $performer,
            'title' => $title,
        ]);
    }

    public static function InputMediaDocument(string $media,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        string|InputFile|null $thumbnail = null,
        ?bool $disable_content_type_detection = null): self
    {
        return new InputMedia(...[
            'type' => 'document',
            'media' => $media,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'thumbnail' => $thumbnail,
            'disable_content_type_detection' => $disable_content_type_detection,
        ]);
    }
}
