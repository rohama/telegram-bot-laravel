<?php

namespace Rohama\Telegram\Type\Messages\Media;

class PhotoSize extends Media
{
    public function __construct(public int $width,
        public int $height,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
