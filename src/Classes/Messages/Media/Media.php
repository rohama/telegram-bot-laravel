<?php

namespace Rohama\Telegram\Type\Messages\Media;

use Rohama\Telegram\Type\TObj;

class Media extends TObj
{
    public function __construct(public string $file_id,
        public string $file_unique_id,
        public ?int $file_size = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
