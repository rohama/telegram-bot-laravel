<?php

namespace Rohama\Telegram\Type\Messages\Media;

use Rohama\Telegram\Type\TObj;

class PaidMediaInfo extends TObj
{
    public array $paid_media;

    public function __construct(public int $star_count,
        array $paid_media,
        ...$args)
    {
        parent::__construct(...$args);
        $this->paid_media = array_map(function ($media) {
            return is_array($media) ? new PaidMedia(...$media) : $media;
        }, $paid_media);
    }
}
