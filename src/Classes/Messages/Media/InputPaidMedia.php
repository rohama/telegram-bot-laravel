<?php

namespace Rohama\Telegram\Type\Messages\Media;

use Rohama\Telegram\Type\InputFile;
use Rohama\Telegram\Type\TObj;

class InputPaidMedia extends TObj
{
    public function __construct(public string $type,
        public string $media,
        public string|InputFile|null $thumbnail = null,
        public ?int $width = null,
        public ?int $height = null,
        public ?int $duration = null,
        public ?bool $supports_streaming = null,
        ...$args)
    {
        parent::__construct(...$args);
    }

    public static function InputPaidMediaPhoto(string $media): self
    {
        return new InputPaidMedia(...[
            'type' => 'photo',
            'media' => $media,
        ]);
    }

    public static function InputPaidMediaVideo(string $media,
        string|InputFile|null $thumbnail = null,
        ?int $width = null,
        ?int $height = null,
        ?int $duration = null,
        ?bool $supports_streaming = null): self
    {
        return new InputPaidMedia(...[
            'type' => 'video',
            'media' => $media,
            'thumbnail' => $thumbnail,
            'width' => $width,
            'height' => $height,
            'duration' => $duration,
            'supports_streaming' => $supports_streaming,
        ]);
    }
}
