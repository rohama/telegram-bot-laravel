<?php

namespace Rohama\Telegram\Type\Messages\Media;

class Voice extends Media
{
    public function __construct(public int $duration,
        public ?string $mime_type = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
