<?php

namespace Rohama\Telegram\Type\Messages\Media;

class Document extends Media
{
    public ?PhotoSize $thumbnail;

    public function __construct(public ?string $file_name = null,
        public ?string $mime_type = null,
        array|PhotoSize|null $thumbnail = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->thumbnail = is_array($thumbnail) ? new PhotoSize(...$thumbnail) : $thumbnail;
    }
}
