<?php

namespace Rohama\Telegram\Type\Messages\Media;

use Rohama\Telegram\Type\TObj;

class PaidMedia extends TObj
{
    public ?array $photo;

    public ?Video $video;

    public function __construct(public string $type,
        public ?int $width = null,
        public ?int $height = null,
        public ?int $duration = null,
        ?array $photo = null,
        array|Video|null $video = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->video = is_array($video) ? new Video(...$video) : $video;
        $this->photo = is_null($photo) ? null : array_map(function ($photo_size) {
            return is_array($photo_size) ? new PhotoSize(...$photo_size) : $photo_size;
        }, $photo);
    }
}
