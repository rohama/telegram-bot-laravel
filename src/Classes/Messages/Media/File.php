<?php

namespace Rohama\Telegram\Type\Messages\Media;

class File extends Media
{
    public function __construct(public ?string $file_path = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
