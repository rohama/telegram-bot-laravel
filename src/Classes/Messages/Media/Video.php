<?php

namespace Rohama\Telegram\Type\Messages\Media;

class Video extends Media
{
    public ?PhotoSize $thumbnail;

    public function __construct(public int $width,
        public int $height,
        public int $duration,
        array|PhotoSize|null $thumbnail = null,
        public ?string $file_name = null,
        public ?string $mime_type = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->thumbnail = is_array($thumbnail) ? new PhotoSize(...$thumbnail) : $thumbnail;
    }
}
