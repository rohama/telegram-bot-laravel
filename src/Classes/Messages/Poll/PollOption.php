<?php

namespace Rohama\Telegram\Type\Messages\Poll;

use Rohama\Telegram\Type\Messages\MessageEntity;
use Rohama\Telegram\Type\TObj;

class PollOption extends TObj
{
    public ?array $text_entities = null;

    public function __construct(public string $text,
        public int $voter_count,
        ?array $text_entities = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->text_entities = is_null($text_entities) ? null : array_map(function ($text_entity) {
            return is_array($text_entity) ? new MessageEntity(...$text_entity) : $text_entity;
        }, $text_entities);
    }
}
