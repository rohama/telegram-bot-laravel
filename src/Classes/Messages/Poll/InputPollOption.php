<?php

namespace Rohama\Telegram\Type\Messages\Poll;

use Rohama\Telegram\Type\Messages\MessageEntity;
use Rohama\Telegram\Type\TObj;

class InputPollOption extends TObj
{
    public ?array $text_entities;

    public function __construct(public string $text,
        public ?string $text_parse_mode = null,
        ?array $text_entities = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->text_entities = is_null($text_entities) ? null : array_map(function ($entity) {
            return is_array($entity) ? new MessageEntity(...$entity) : $entity;
        }, $text_entities);
    }
}
