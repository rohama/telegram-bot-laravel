<?php

namespace Rohama\Telegram\Type\Messages\Poll;

use Rohama\Telegram\Type\Messages\MessageEntity;
use Rohama\Telegram\Type\TObj;

class Poll extends TObj
{
    public array $options;

    public ?array $question_entities;

    public ?array $explanation_entities;

    public function __construct(public string $id,
        public string $question,
        array $options,
        public int $total_voter_count,
        public bool $is_closed,
        public bool $is_anonymous,
        public string $type,
        public bool $allows_multiple_answers,
        ?array $question_entities = null,
        public ?int $correct_option_id = null,
        public ?string $explanation = null,
        ?array $explanation_entities = null,
        public ?int $open_period = null,
        public ?int $close_date = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->options = array_map(function ($option) {
            return is_array($option) ? new PollOption(...$option) : $option;
        }, $options);
        $this->question_entities = is_null($question_entities) ? null : array_map(function ($entity) {
            return is_array($entity) ? new MessageEntity(...$entity) : $entity;
        }, $question_entities);
        $this->explanation_entities = is_null($question_entities) ? null : array_map(function ($entity) {
            return is_array($entity) ? new MessageEntity(...$entity) : $entity;
        }, $explanation_entities);
    }
}
