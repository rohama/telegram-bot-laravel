<?php

namespace Rohama\Telegram\Type\Messages\Poll;

use Rohama\Telegram\Type\Chats\Chat;
use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\TObj;

class PollAnswer extends TObj
{
    public ?Chat $voter_chat;

    public ?User $user;

    public function __construct(public string $text,
        public array $option_ids,
        array|Chat|null $voter_chat = null,
        array|User|null $user = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->voter_chat = is_array($voter_chat) ? new Chat(...$voter_chat) : $voter_chat;
        $this->user = is_array($user) ? new User(...$user) : $user;
    }
}
