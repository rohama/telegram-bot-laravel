<?php

namespace Rohama\Telegram\Type\Messages;

use Rohama\Telegram\Type\TObj;
use Rohama\Telegram\Type\Update\Message;

class MaybeInaccessibleMessage extends TObj
{
    public bool $inaccessible;

    public Message|InaccessibleMessage $message;

    public function __construct(...$args)
    {
        if ($args['date'] === 0) {
            $this->inaccessible = true;
            $this->message = new InaccessibleMessage(...$args);
        } else {
            $this->inaccessible = false;
            $this->message = new Message(...$args);
        }
    }
}
