<?php

namespace Rohama\Telegram\Type\Messages;

use Rohama\Telegram\Type\TObj;

class Location extends TObj
{
    public function __construct(public float $longitude,
        public float $latitude,
        public ?float $horizontal_accuracy = null,
        public ?int $live_period = null,
        public ?int $heading = null,
        public ?int $proximity_alert_radius = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
