<?php

namespace Rohama\Telegram\Type\Messages;

use Rohama\Telegram\Type\Chats\Chat;
use Rohama\Telegram\Type\Game\Game;
use Rohama\Telegram\Type\Messages\Giveaway\Giveaway;
use Rohama\Telegram\Type\Messages\Giveaway\GiveawayWinners;
use Rohama\Telegram\Type\Messages\Media\Animation;
use Rohama\Telegram\Type\Messages\Media\Audio;
use Rohama\Telegram\Type\Messages\Media\Document;
use Rohama\Telegram\Type\Messages\Media\PaidMediaInfo;
use Rohama\Telegram\Type\Messages\Media\PhotoSize;
use Rohama\Telegram\Type\Messages\Media\Story;
use Rohama\Telegram\Type\Messages\Media\Video;
use Rohama\Telegram\Type\Messages\Media\VideoNote;
use Rohama\Telegram\Type\Messages\Media\Voice;
use Rohama\Telegram\Type\Messages\Poll\Poll;
use Rohama\Telegram\Type\Payment\Invoice;
use Rohama\Telegram\Type\Sticker\Sticker;
use Rohama\Telegram\Type\TObj;

class ExternalReplyInfo extends TObj
{
    public MessageOrigin $origin;

    public ?Chat $chat;

    public ?LinkPreviewOptions $link_preview_options;

    public ?Animation $animation;

    public ?Audio $audio;

    public ?Document $document;

    public ?PaidMediaInfo $paid_media;

    public ?array $photo;

    public ?Sticker $sticker;

    public ?Story $story;

    public ?Video $video;

    public ?VideoNote $video_note;

    public ?Voice $voice;

    public ?Contact $contact;

    public ?Dice $dice;

    public ?Game $game;

    public ?Giveaway $giveaway;

    public ?GiveawayWinners $giveaway_winners;

    public ?Invoice $invoice;

    public ?Location $location;

    public ?Poll $poll;

    public ?Venue $venue;

    public function __construct(MessageOrigin|array $origin,
        array|Chat|null $chat = null,
        public ?int $message_id = null,
        array|LinkPreviewOptions|null $link_preview_options = null,
        array|Animation|null $animation = null,
        array|Audio|null $audio = null,
        array|Document|null $document = null,
        array|PaidMediaInfo|null $paid_media = null,
        ?array $photo = null,
        array|Sticker|null $sticker = null,
        array|Story|null $story = null,
        array|Video|null $video = null,
        array|VideoNote|null $video_note = null,
        array|Voice|null $voice = null,
        public ?bool $has_media_spoiler = null,
        array|Contact|null $contact = null,
        array|Dice|null $dice = null,
        array|Game|null $game = null,
        array|Giveaway|null $giveaway = null,
        array|GiveawayWinners|null $giveaway_winners = null,
        array|Invoice|null $invoice = null,
        array|Location|null $location = null,
        array|Poll|null $poll = null,
        array|Venue|null $venue = null,
        ...$args)
    {
        parent::__construct(...$args);
        $this->origin = is_array($origin) ? new MessageOrigin(...$origin) : $origin;
        $this->chat = is_array($chat) ? new Chat(...$chat) : $chat;
        $this->link_preview_options = is_array($link_preview_options) ? new LinkPreviewOptions(...$link_preview_options) : $link_preview_options;
        $this->animation = is_array($animation) ? new Animation(...$animation) : $animation;
        $this->audio = is_array($audio) ? new Audio(...$audio) : $audio;
        $this->document = is_array($document) ? new Document(...$document) : $document;
        $this->paid_media = is_array($paid_media) ? new PaidMediaInfo(...$paid_media) : $paid_media;
        $this->photo = is_null($photo) ? null : array_map(function ($photo_size) {
            return is_array($photo_size) ? new PhotoSize(...$photo_size) : $photo_size;
        }, $photo);
        $this->sticker = is_array($sticker) ? new Sticker(...$sticker) : $sticker;
        $this->story = is_array($story) ? new Story(...$story) : $story;
        $this->video = is_array($video) ? new Video(...$video) : $video;
        $this->video_note = is_array($video_note) ? new VideoNote(...$video_note) : $video_note;
        $this->voice = is_array($voice) ? new Voice(...$voice) : $voice;
        $this->contact = is_array($contact) ? new Contact(...$contact) : $contact;
        $this->dice = is_array($dice) ? new Dice(...$dice) : $dice;
        $this->game = is_array($game) ? new Game(...$game) : $game;
        $this->giveaway = is_array($giveaway) ? new Giveaway(...$giveaway) : $giveaway;
        $this->giveaway_winners = is_array($giveaway_winners) ? new GiveawayWinners(...$giveaway_winners) : $giveaway_winners;
        $this->invoice = is_array($invoice) ? new Invoice(...$invoice) : $invoice;
        $this->location = is_array($location) ? new Location(...$location) : $location;
        $this->poll = is_array($poll) ? new Poll(...$poll) : $poll;
        $this->venue = is_array($venue) ? new Venue(...$venue) : $venue;
    }
}
