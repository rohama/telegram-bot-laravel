<?php

namespace Rohama\Telegram\Type\Messages;

use Rohama\Telegram\Type\TObj;

class ForceReply extends TObj
{
    public bool $force_reply = true;

    public function __construct(public ?string $input_field_placeholder = null,
        public ?bool $selective = null,
        ...$args)
    {
        parent::__construct(...$args);
    }
}
