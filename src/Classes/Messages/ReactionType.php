<?php

namespace Rohama\Telegram\Type\Messages;

use Rohama\Telegram\Type\TObj;

class ReactionType extends TObj
{
    public function __construct(public string $type,
        public ?string $emoji = null,
        public ?string $custom_emoji_id = null,
        ...$args)
    {
        parent::__construct(...$args);
    }

    public static function ReactionTypeEmoji(string $emoji): self
    {
        return new ReactionType(...[
            'type' => 'emoji',
            'emoji' => $emoji,
        ]);
    }

    public static function ReactionTypeCustomEmoji(string $custom_emoji_id): self
    {
        return new ReactionType(...[
            'type' => 'custom_emoji',
            'custom_emoji_id' => $custom_emoji_id,
        ]);
    }
}
