<?php

use Rohama\Telegram\Type\Game\CallbackGame;
use Rohama\Telegram\Type\Messages\Keyboard\InlineKeyboardButton;
use Rohama\Telegram\Type\Messages\Keyboard\KeyboardButton;
use Rohama\Telegram\Type\Messages\Keyboard\KeyboardButtonPollType;
use Rohama\Telegram\Type\Messages\Keyboard\KeyboardButtonRequestChat;
use Rohama\Telegram\Type\Messages\Keyboard\KeyboardButtonRequestUsers;
use Rohama\Telegram\Type\Messages\Keyboard\LoginUrl;
use Rohama\Telegram\Type\Messages\Keyboard\SwitchInlineQueryChosenChat;
use Rohama\Telegram\Type\Messages\WebAppInfo;
use Rohama\Telegram\Type\TObj;

function t_inlineButton(string $text,
    ?string $url = null,
    ?string $callback_data = null,
    array|WebAppInfo|null $web_app = null,
    array|LoginUrl|null $login_url = null,
    ?string $switch_inline_query = null,
    ?string $switch_inline_query_current_chat = null,
    array|SwitchInlineQueryChosenChat|null $switch_inline_query_chosen_chat = null,
    array|CallbackGame|null $callback_game = null,
    ?bool $pay = null,
    ...$args): InlineKeyboardButton
{
    return new InlineKeyboardButton($text, $url, $callback_data, $web_app, $login_url, $switch_inline_query, $switch_inline_query_current_chat, $switch_inline_query_chosen_chat, $callback_game, $pay, ...$args);
}

function t_keyboardButton(string $text,
    array|KeyboardButtonRequestUsers|null $request_users = null,
    array|KeyboardButtonRequestChat|null $request_chat = null,
    ?bool $request_contact = null,
    ?bool $request_location = null,
    array|KeyboardButtonPollType|null $request_poll = null,
    array|WebAppInfo|null $web_app = null,
    ...$args): KeyboardButton
{
    return new KeyboardButton($text, $request_users, $request_chat, $request_contact, $request_location, $request_poll, $web_app, ...$args);
}

function t_toArray($array): mixed
{
    if ($array instanceof TObj) {
        return t_toArray($array->toArray());
    } elseif (! is_array($array)) {
        return $array;
    } else {
        return array_map(function ($value) {
            return $value instanceof TObj ? t_toArray($value->toArray()) : t_toArray($value);
        }, array_filter($array));
    }
}
