<?php

namespace Rohama\Telegram;

use GuzzleHttp\Client;
use Rohama\Telegram\Traits\bot;
use Rohama\Telegram\Traits\chat;
use Rohama\Telegram\Traits\edit;
use Rohama\Telegram\Traits\game;
use Rohama\Telegram\Traits\inline;
use Rohama\Telegram\Traits\passport;
use Rohama\Telegram\Traits\payment;
use Rohama\Telegram\Traits\send;
use Rohama\Telegram\Traits\sticker;
use Rohama\Telegram\Traits\update;
use Rohama\Telegram\Type\InputFile;

class Telegram
{
    private Client $client;

    public function __construct(private string $token,
        string $api = 'https://api.telegram.org/bot')
    {
        $this->client = new Client(['base_uri' => $api.$token.'/']);
    }

    /**
     * Make Request to endpoint
     *
     * @param  string  $method  Endpoint
     * @param  array  $data  The Request Body
     * @return mixed Response
     */
    public function bot(string $method, array $data = [], array $files = [])
    {
        if ($files === []) {
            $response = $this->client->request('POST', $method, ['json' => t_toArray($data)]);
        } else {
            $response = $this->client->request('POST', $method, ['multipart' => self::toMultipart($data, $files)]);
        }

        return json_decode($response->getBody(), true)['result'];
    }

    private static function toMultipart(array $data, array $files): array
    {
        $multipart = [];

        foreach ($data as $key => $value) {
            $multipart[] = [
                'name' => $key,
                'contents' => $value,
            ];
        }

        foreach ($files as $key => $value) {
            if ($value instanceof InputFile) {
                $value = $value->toMultipart();
                if ($value) {
                    $multipart[] = $value;
                }
            } else {
                $multipart[] = [
                    'name' => $key,
                    'contents' => $value,
                ];
            }
        }

        return $multipart;
    }

    use bot, chat, edit, game, inline, passport, payment, send, sticker, update;
}
