<?php

namespace Rohama\Telegram\Traits;

trait passport
{
    public function setPassportDataErrors(int $user_id,
        array $errors): bool
    {
        return $this->bot('setPassportDataErrors', [
            'user_id' => $user_id,
            'errors' => $errors,
        ]);
    }
}
