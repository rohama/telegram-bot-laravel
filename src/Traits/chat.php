<?php

namespace Rohama\Telegram\Traits;

use Rohama\Telegram\Type\Chats\Chat as ChatObj;
use Rohama\Telegram\Type\Chats\ChatInviteLink;
use Rohama\Telegram\Type\Chats\ChatMember;
use Rohama\Telegram\Type\Chats\ChatPermissions;
use Rohama\Telegram\Type\Chats\ForumTopic;
use Rohama\Telegram\Type\Chats\UserChatBoosts;
use Rohama\Telegram\Type\Chats\UserProfilePhotos;
use Rohama\Telegram\Type\InputFile;
use Rohama\Telegram\Type\Sticker\Sticker;

trait chat
{
    public function getUserProfilePhotos(int $user_id,
        ?int $offset = null,
        ?int $limit = null): UserProfilePhotos
    {
        return new UserProfilePhotos(...$this->bot('getUserProfilePhotos', [
            'user_id' => $user_id,
            'offset' => $offset,
            'limit' => $limit,
        ]));
    }

    public function banChatMember(int|string $chat_id,
        int $user_id,
        ?int $until_date = null,
        ?bool $revoke_messages = null): bool
    {
        return $this->bot('banChatMember', [
            'chat_id' => $chat_id,
            'user_id' => $user_id,
            'until_date' => $until_date,
            'revoke_messages' => $revoke_messages,
        ]);
    }

    public function unbanChatMember(int|string $chat_id,
        int $user_id,
        ?bool $only_if_banned = null): bool
    {
        return $this->bot('unbanChatMember', [
            'chat_id' => $chat_id,
            'user_id' => $user_id,
            'only_if_banned' => $only_if_banned,
        ]);
    }

    public function restrictChatMember(int|string $chat_id,
        int $user_id,
        array|ChatPermissions $permissions,
        ?bool $use_independent_chat_permissions = null,
        ?int $until_date = null): bool
    {
        return $this->bot('restrictChatMember', [
            'chat_id' => $chat_id,
            'user_id' => $user_id,
            'permissions' => $permissions,
            'use_independent_chat_permissions' => $use_independent_chat_permissions,
            'until_date' => $until_date,
        ]);
    }

    public function promoteChatMember(int|string $chat_id,
        int $user_id,
        ?bool $is_anonymous = null,
        ?bool $can_manage_chat = null,
        ?bool $can_delete_messages = null,
        ?bool $can_manage_video_chats = null,
        ?bool $can_restrict_members = null,
        ?bool $can_promote_members = null,
        ?bool $can_change_info = null,
        ?bool $can_invite_users = null,
        ?bool $can_post_stories = null,
        ?bool $can_edit_stories = null,
        ?bool $can_delete_stories = null,
        ?bool $can_post_messages = null,
        ?bool $can_edit_messages = null,
        ?bool $can_pin_messages = null,
        ?bool $can_manage_topics = null): bool
    {
        return $this->bot('promoteChatMember', [
            'chat_id' => $chat_id,
            'user_id' => $user_id,
            'is_anonymous' => $is_anonymous,
            'can_manage_chat' => $can_manage_chat,
            'can_delete_messages' => $can_delete_messages,
            'can_manage_video_chats' => $can_manage_video_chats,
            'can_restrict_members' => $can_restrict_members,
            'can_promote_members' => $can_promote_members,
            'can_change_info' => $can_change_info,
            'can_invite_users' => $can_invite_users,
            'can_post_stories' => $can_post_stories,
            'can_edit_stories' => $can_edit_stories,
            'can_delete_stories' => $can_delete_stories,
            'can_post_messages' => $can_post_messages,
            'can_edit_messages' => $can_edit_messages,
            'can_pin_messages' => $can_pin_messages,
            'can_manage_topics' => $can_manage_topics,
        ]);
    }

    public function setChatAdministratorCustomTitle(int|string $chat_id,
        int $user_id,
        string $custom_title): bool
    {
        return $this->bot('setChatAdministratorCustomTitle', [
            'chat_id' => $chat_id,
            'user_id' => $user_id,
            'custom_title' => $custom_title,
        ]);
    }

    public function banChatSenderChat(int|string $chat_id,
        int $sender_chat_id): bool
    {
        return $this->bot('banChatSenderChat', [
            'chat_id' => $chat_id,
            'sender_chat_id' => $sender_chat_id,
        ]);
    }

    public function unbanChatSenderChat(int|string $chat_id,
        int $sender_chat_id): bool
    {
        return $this->bot('unbanChatSenderChat', [
            'chat_id' => $chat_id,
            'sender_chat_id' => $sender_chat_id,
        ]);
    }

    public function setChatPermissions(int|string $chat_id,
        array|ChatPermissions $permissions,
        ?bool $use_independent_chat_permissions = null): bool
    {
        return $this->bot('setChatPermissions', [
            'chat_id' => $chat_id,
            'permissions' => $permissions,
            'use_independent_chat_permissions' => $use_independent_chat_permissions,
        ]);
    }

    public function exportChatInviteLink(int|string $chat_id): string
    {
        return $this->bot('exportChatInviteLink', [
            'chat_id' => $chat_id,
        ]);
    }

    public function createChatInviteLink(int|string $chat_id,
        ?string $name = null,
        ?int $expire_date = null,
        ?int $member_limit = null,
        ?bool $creates_join_request = null): ChatInviteLink
    {
        return new ChatInviteLink(...$this->bot('createChatInviteLink', [
            'chat_id' => $chat_id,
            'name' => $name,
            'expire_date' => $expire_date,
            'member_limit' => $member_limit,
            'creates_join_request' => $creates_join_request,
        ]));
    }

    public function editChatInviteLink(int|string $chat_id,
        string $invite_link,
        ?string $name = null,
        ?int $expire_date = null,
        ?int $member_limit = null,
        ?bool $creates_join_request = null): ChatInviteLink
    {
        return new ChatInviteLink(...$this->bot('editChatInviteLink', [
            'chat_id' => $chat_id,
            'invite_link' => $invite_link,
            'name' => $name,
            'expire_date' => $expire_date,
            'member_limit' => $member_limit,
            'creates_join_request' => $creates_join_request,
        ]));
    }

    public function createChatSubscriptionInviteLink(int|string $chat_id,
        int $subscription_period,
        int $subscription_price,
        ?string $name = null): ChatInviteLink
    {
        return new ChatInviteLink(...$this->bot('createChatSubscriptionInviteLink', [
            'chat_id' => $chat_id,
            'subscription_period' => $subscription_period,
            'subscription_price' => $subscription_price,
            'name' => $name,
        ]));
    }

    public function editChatSubscriptionInviteLink(int|string $chat_id,
        string $invite_link,
        ?string $name = null): ChatInviteLink
    {
        return new ChatInviteLink(...$this->bot('editChatSubscriptionInviteLink', [
            'chat_id' => $chat_id,
            'invite_link' => $invite_link,
            'name' => $name,
        ]));
    }

    public function revokeChatInviteLink(int|string $chat_id,
        string $invite_link): ChatInviteLink
    {
        return new ChatInviteLink(...$this->bot('revokeChatInviteLink', [
            'chat_id' => $chat_id,
            'invite_link' => $invite_link,
        ]));
    }

    public function approveChatJoinRequest(int|string $chat_id,
        int $user_id): bool
    {
        return $this->bot('approveChatJoinRequest', [
            'chat_id' => $chat_id,
            'user_id' => $user_id,
        ]);
    }

    public function declineChatJoinRequest(int|string $chat_id,
        int $user_id): bool
    {
        return $this->bot('declineChatJoinRequest', [
            'chat_id' => $chat_id,
            'user_id' => $user_id,
        ]);
    }

    public function setChatPhoto(int|string $chat_id,
        InputFile $photo): bool
    {
        return $this->bot('setChatPhoto', [
            'chat_id' => $chat_id,
            'photo' => $photo,
        ]);
    }

    public function deleteChatPhoto(int|string $chat_id): bool
    {
        return $this->bot('deleteChatPhoto', [
            'chat_id' => $chat_id,
        ]);
    }

    public function setChatTitle(int|string $chat_id,
        string $title): bool
    {
        return $this->bot('setChatTitle', [
            'chat_id' => $chat_id,
            'title' => $title,
        ]);
    }

    public function setChatDescription(int|string $chat_id,
        string $description): bool
    {
        return $this->bot('setChatDescription', [
            'chat_id' => $chat_id,
            'description' => $description,
        ]);
    }

    public function pinChatMessage(int|string $chat_id,
        int $message_id,
        ?string $business_connection_id = null,
        ?bool $disable_notification = null): bool
    {
        return $this->bot('pinChatMessage', [
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'business_connection_id' => $business_connection_id,
            'disable_notification' => $disable_notification,
        ]);
    }

    public function unpinChatMessage(int|string $chat_id,
        ?int $message_id = null,
        ?string $business_connection_id = null): bool
    {
        return $this->bot('unpinChatMessage', [
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'business_connection_id' => $business_connection_id,
        ]);
    }

    public function unpinAllChatMessages(int|string $chat_id): bool
    {
        return $this->bot('unpinAllChatMessages', [
            'chat_id' => $chat_id,
        ]);
    }

    public function leaveChat(int|string $chat_id): bool
    {
        return $this->bot('leaveChat', [
            'chat_id' => $chat_id,
        ]);
    }

    public function getChat(int|string $chat_id): ChatObj
    {
        return new ChatObj(...$this->bot('getChat', [
            'chat_id' => $chat_id,
        ]));
    }

    public function getChatAdministrators(int|string $chat_id): array
    {
        $admins = $this->bot('getChatAdministrators', [
            'chat_id' => $chat_id,
        ]);

        return array_map(function ($admin) {
            return new ChatMember(...$admin);
        }, $admins);
    }

    public function getChatMemberCount(int|string $chat_id): int
    {
        return $this->bot('getChatMemberCount', [
            'chat_id' => $chat_id,
        ]);
    }

    public function getChatMember(int|string $chat_id,
        int $user_id): ChatMember
    {
        return new ChatMember(...$this->bot('getChatMember', [
            'chat_id' => $chat_id,
            'user_id' => $user_id,
        ]));
    }

    public function setChatStickerSet(int|string $chat_id,
        string $sticker_set_name): bool
    {
        return $this->bot('setChatStickerSet', [
            'chat_id' => $chat_id,
            'sticker_set_name' => $sticker_set_name,
        ]);
    }

    public function deleteChatStickerSet(int|string $chat_id): bool
    {
        return $this->bot('deleteChatStickerSet', [
            'chat_id' => $chat_id,
        ]);
    }

    public function getForumTopicIconStickers(): array
    {
        $stickers = $this->bot('getForumTopicIconStickers');

        return array_map(function ($sticker) {
            return new Sticker(...$sticker);
        }, $stickers);
    }

    public function createForumTopic(int|string $chat_id,
        string $name,
        ?int $icon_color = null,
        ?string $icon_custom_emoji_id = null): ForumTopic
    {
        return new ForumTopic(...$this->bot('createForumTopic', [
            'chat_id' => $chat_id,
            'name' => $name,
            'icon_color' => $icon_color,
            'icon_custom_emoji_id' => $icon_custom_emoji_id,
        ]));
    }

    public function editForumTopic(int|string $chat_id,
        int $message_thread_id,
        ?string $name = null,
        ?string $icon_custom_emoji_id = null): bool
    {
        return $this->bot('editForumTopic', [
            'chat_id' => $chat_id,
            'message_thread_id' => $message_thread_id,
            'name' => $name,
            'icon_custom_emoji_id' => $icon_custom_emoji_id,
        ]);
    }

    public function closeForumTopic(int|string $chat_id,
        int $message_thread_id): bool
    {
        return $this->bot('closeForumTopic', [
            'chat_id' => $chat_id,
            'message_thread_id' => $message_thread_id,
        ]);
    }

    public function reopenForumTopic(int|string $chat_id,
        int $message_thread_id): bool
    {
        return $this->bot('reopenForumTopic', [
            'chat_id' => $chat_id,
            'message_thread_id' => $message_thread_id,
        ]);
    }

    public function deleteForumTopic(int|string $chat_id,
        int $message_thread_id): bool
    {
        return $this->bot('deleteForumTopic', [
            'chat_id' => $chat_id,
            'message_thread_id' => $message_thread_id,
        ]);
    }

    public function unpinAllForumTopicMessages(int|string $chat_id,
        int $message_thread_id): bool
    {
        return $this->bot('unpinAllForumTopicMessages', [
            'chat_id' => $chat_id,
            'message_thread_id' => $message_thread_id,
        ]);
    }

    public function editGeneralForumTopic(int|string $chat_id,
        string $name): bool
    {
        return $this->bot('editGeneralForumTopic', [
            'chat_id' => $chat_id,
            'name' => $name,
        ]);
    }

    public function closeGeneralForumTopic(int|string $chat_id): bool
    {
        return $this->bot('closeGeneralForumTopic', [
            'chat_id' => $chat_id,
        ]);
    }

    public function reopenGeneralForumTopic(int|string $chat_id): bool
    {
        return $this->bot('reopenGeneralForumTopic', [
            'chat_id' => $chat_id,
        ]);
    }

    public function hideGeneralForumTopic(int|string $chat_id): bool
    {
        return $this->bot('hideGeneralForumTopic', [
            'chat_id' => $chat_id,
        ]);
    }

    public function unhideGeneralForumTopic(int|string $chat_id): bool
    {
        return $this->bot('unhideGeneralForumTopic', [
            'chat_id' => $chat_id,
        ]);
    }

    public function unpinAllGeneralForumTopicMessages(int|string $chat_id): bool
    {
        return $this->bot('unpinAllGeneralForumTopicMessages', [
            'chat_id' => $chat_id,
        ]);
    }

    public function getUserChatBoosts(int|string $chat_id,
        int $user_id): UserChatBoosts
    {
        return new UserChatBoosts(...$this->bot('getUserChatBoosts', [
            'chat_id' => $chat_id,
            'user_id' => $user_id,
        ]));
    }
}
