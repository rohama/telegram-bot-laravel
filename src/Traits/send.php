<?php

namespace Rohama\Telegram\Traits;

use Rohama\Telegram\Type\InputFile;
use Rohama\Telegram\Type\Messages\ForceReply;
use Rohama\Telegram\Type\Messages\Keyboard\InlineKeyboardMarkup;
use Rohama\Telegram\Type\Messages\Keyboard\ReplyKeyboardMarkup;
use Rohama\Telegram\Type\Messages\Keyboard\ReplyKeyboardRemove;
use Rohama\Telegram\Type\Messages\LinkPreviewOptions;
use Rohama\Telegram\Type\Messages\MessageId;
use Rohama\Telegram\Type\Messages\ReplyParameters;
use Rohama\Telegram\Type\Update\Message;

trait send
{
    public function sendMessage(string|int $chat_id,
        string $text,
        ?string $business_connection_id = null,
        ?int $message_thread_id = null,
        ?string $parse_mode = null,
        ?array $entities = null,
        array|LinkPreviewOptions|null $link_preview_options = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null,
        ?string $message_effect_id = null,
        array|ReplyParameters|null $reply_parameters = null,
        array|InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply|null $reply_markup = null): Message
    {
        return new Message(...$this->bot('sendMessage', [
            'chat_id' => $chat_id,
            'text' => $text,
            'business_connection_id' => $business_connection_id,
            'message_thread_id' => $message_thread_id,
            'parse_mode' => $parse_mode,
            'entities' => $entities,
            'link_preview_options' => $link_preview_options,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
            'message_effect_id' => $message_effect_id,
            'reply_parameters' => $reply_parameters,
            'reply_markup' => $reply_markup,
        ]));
    }

    public function forwardMessage(string|int $chat_id,
        string|int $from_chat_id,
        int $message_id,
        ?int $message_thread_id = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null): Message
    {
        return new Message(...$this->bot('forwardMessage', [
            'chat_id' => $chat_id,
            'from_chat_id' => $from_chat_id,
            'message_id' => $message_id,
            'message_thread_id' => $message_thread_id,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
        ]));
    }

    public function forwardMessages(string|int $chat_id,
        string|int $from_chat_id,
        array $message_ids,
        ?int $message_thread_id = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null): array
    {
        $messages = $this->bot('forwardMessages', [
            'chat_id' => $chat_id,
            'from_chat_id' => $from_chat_id,
            'message_ids' => $message_ids,
            'message_thread_id' => $message_thread_id,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
        ]);

        return array_map(function ($message) {
            return new MessageId(...$message);
        }, $messages);
    }

    public function copyMessage(string|int $chat_id,
        string|int $from_chat_id,
        int $message_id,
        ?int $message_thread_id = null,
        ?string $caption = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        ?bool $show_caption_above_media = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null,
        array|ReplyParameters|null $reply_parameters = null,
        array|InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply|null $reply_markup = null): MessageId
    {
        return new MessageId(...$this->bot('copyMessage', [
            'chat_id' => $chat_id,
            'from_chat_id' => $from_chat_id,
            'message_id' => $message_id,
            'message_thread_id' => $message_thread_id,
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'show_caption_above_media' => $show_caption_above_media,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
            'reply_parameters' => $reply_parameters,
            'reply_markup' => $reply_markup,
        ]));
    }

    public function copyMessages(string|int $chat_id,
        string|int $from_chat_id,
        array $message_ids,
        ?int $message_thread_id = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null,
        ?bool $remove_caption = null): array
    {
        $messages = $this->bot('copyMessages', [
            'chat_id' => $chat_id,
            'from_chat_id' => $from_chat_id,
            'message_ids' => $message_ids,
            'message_thread_id' => $message_thread_id,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
            'remove_caption' => $remove_caption,
        ]);

        return array_map(function ($message) {
            return new MessageId(...$message);
        }, $messages);
    }

    public function sendPhoto(string|int $chat_id,
        string|InputFile $photo,
        ?string $caption = null,
        ?string $business_connection_id = null,
        ?int $message_thread_id = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        ?bool $show_caption_above_media = null,
        ?bool $has_spoiler = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null,
        ?string $message_effect_id = null,
        array|ReplyParameters|null $reply_parameters = null,
        array|InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply|null $reply_markup = null): Message
    {
        return new Message(...$this->bot('sendPhoto', [
            'chat_id' => $chat_id,
            'photo' => $photo,
            'caption' => $caption,
            'business_connection_id' => $business_connection_id,
            'message_thread_id' => $message_thread_id,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'show_caption_above_media' => $show_caption_above_media,
            'has_spoiler' => $has_spoiler,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
            'message_effect_id' => $message_effect_id,
            'reply_parameters' => $reply_parameters,
            'reply_markup' => $reply_markup,
        ]));
    }

    public function sendAudio(string|int $chat_id,
        string|InputFile $audio,
        ?string $caption = null,
        ?string $business_connection_id = null,
        ?int $message_thread_id = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        ?int $duration = null,
        ?string $performer = null,
        ?string $title = null,
        string|InputFile|null $thumbnail = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null,
        ?string $message_effect_id = null,
        array|ReplyParameters|null $reply_parameters = null,
        array|InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply|null $reply_markup = null): Message
    {
        return new Message(...$this->bot('sendAudio', [
            'chat_id' => $chat_id,
            'audio' => $audio,
            'caption' => $caption,
            'business_connection_id' => $business_connection_id,
            'message_thread_id' => $message_thread_id,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'duration' => $duration,
            'performer' => $performer,
            'title' => $title,
            'thumbnail' => $thumbnail,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
            'message_effect_id' => $message_effect_id,
            'reply_parameters' => $reply_parameters,
            'reply_markup' => $reply_markup,
        ]));
    }

    public function sendDocument(string|int $chat_id,
        string|InputFile $document,
        ?string $caption = null,
        ?string $business_connection_id = null,
        ?int $message_thread_id = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        ?int $disable_content_type_detection = null,
        string|InputFile|null $thumbnail = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null,
        ?string $message_effect_id = null,
        array|ReplyParameters|null $reply_parameters = null,
        array|InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply|null $reply_markup = null): Message
    {
        return new Message(...$this->bot('sendDocument', [
            'chat_id' => $chat_id,
            'document' => $document,
            'caption' => $caption,
            'business_connection_id' => $business_connection_id,
            'message_thread_id' => $message_thread_id,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'disable_content_type_detection' => $disable_content_type_detection,
            'thumbnail' => $thumbnail,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
            'message_effect_id' => $message_effect_id,
            'reply_parameters' => $reply_parameters,
            'reply_markup' => $reply_markup,
        ]));
    }

    public function sendVideo(string|int $chat_id,
        string|InputFile $video,
        ?string $caption = null,
        ?string $business_connection_id = null,
        ?int $message_thread_id = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        ?int $duration = null,
        ?int $width = null,
        ?int $height = null,
        string|InputFile|null $thumbnail = null,
        ?bool $show_caption_above_media = null,
        ?bool $has_spoiler = null,
        ?bool $supports_streaming = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null,
        ?string $message_effect_id = null,
        array|ReplyParameters|null $reply_parameters = null,
        array|InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply|null $reply_markup = null): Message
    {
        return new Message(...$this->bot('sendVideo', [
            'chat_id' => $chat_id,
            'video' => $video,
            'caption' => $caption,
            'business_connection_id' => $business_connection_id,
            'message_thread_id' => $message_thread_id,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'duration' => $duration,
            'width' => $width,
            'height' => $height,
            'thumbnail' => $thumbnail,
            'show_caption_above_media' => $show_caption_above_media,
            'has_spoiler' => $has_spoiler,
            'supports_streaming' => $supports_streaming,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
            'message_effect_id' => $message_effect_id,
            'reply_parameters' => $reply_parameters,
            'reply_markup' => $reply_markup,
        ]));
    }

    public function sendAnimation(string|int $chat_id,
        string|InputFile $animation,
        ?string $caption = null,
        ?string $business_connection_id = null,
        ?int $message_thread_id = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        ?int $duration = null,
        ?int $width = null,
        ?int $height = null,
        string|InputFile|null $thumbnail = null,
        ?bool $show_caption_above_media = null,
        ?bool $has_spoiler = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null,
        ?string $message_effect_id = null,
        array|ReplyParameters|null $reply_parameters = null,
        array|InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply|null $reply_markup = null): Message
    {
        return new Message(...$this->bot('sendAnimation', [
            'chat_id' => $chat_id,
            'animation' => $animation,
            'caption' => $caption,
            'business_connection_id' => $business_connection_id,
            'message_thread_id' => $message_thread_id,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'duration' => $duration,
            'width' => $width,
            'height' => $height,
            'thumbnail' => $thumbnail,
            'show_caption_above_media' => $show_caption_above_media,
            'has_spoiler' => $has_spoiler,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
            'message_effect_id' => $message_effect_id,
            'reply_parameters' => $reply_parameters,
            'reply_markup' => $reply_markup,
        ]));
    }

    public function sendVoice(string|int $chat_id,
        string|InputFile $voice,
        ?string $caption = null,
        ?string $business_connection_id = null,
        ?int $message_thread_id = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        ?int $duration = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null,
        ?string $message_effect_id = null,
        array|ReplyParameters|null $reply_parameters = null,
        array|InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply|null $reply_markup = null): Message
    {
        return new Message(...$this->bot('sendVoice', [
            'chat_id' => $chat_id,
            'voice' => $voice,
            'caption' => $caption,
            'business_connection_id' => $business_connection_id,
            'message_thread_id' => $message_thread_id,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'duration' => $duration,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
            'message_effect_id' => $message_effect_id,
            'reply_parameters' => $reply_parameters,
            'reply_markup' => $reply_markup,
        ]));
    }

    public function sendVideoNote(string|int $chat_id,
        string|InputFile $video_note,
        ?string $business_connection_id = null,
        ?int $message_thread_id = null,
        ?int $duration = null,
        ?int $length = null,
        string|InputFile|null $thumbnail = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null,
        ?string $message_effect_id = null,
        array|ReplyParameters|null $reply_parameters = null,
        array|InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply|null $reply_markup = null): Message
    {
        return new Message(...$this->bot('sendVideoNote', [
            'chat_id' => $chat_id,
            'video_note' => $video_note,
            'business_connection_id' => $business_connection_id,
            'message_thread_id' => $message_thread_id,
            'duration' => $duration,
            'length' => $length,
            'thumbnail' => $thumbnail,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
            'message_effect_id' => $message_effect_id,
            'reply_parameters' => $reply_parameters,
            'reply_markup' => $reply_markup,
        ]));
    }

    public function sendPaidMedia(string|int $chat_id,
        array $media,
        int $star_count,
        ?string $caption = null,
        ?string $business_connection_id = null,
        ?int $message_thread_id = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        ?bool $show_caption_above_media = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null,
        ?string $message_effect_id = null,
        array|ReplyParameters|null $reply_parameters = null,
        array|InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply|null $reply_markup = null): Message
    {
        return new Message(...$this->bot('sendPaidMedia', [
            'chat_id' => $chat_id,
            'media' => $media,
            'star_count' => $star_count,
            'caption' => $caption,
            'business_connection_id' => $business_connection_id,
            'message_thread_id' => $message_thread_id,
            'parse_mode' => $parse_mode,
            'caption_entities' => $caption_entities,
            'show_caption_above_media' => $show_caption_above_media,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
            'message_effect_id' => $message_effect_id,
            'reply_parameters' => $reply_parameters,
            'reply_markup' => $reply_markup,
        ]));
    }

    public function sendMediaGroup(string|int $chat_id,
        array $media,
        ?string $business_connection_id = null,
        ?int $message_thread_id = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null,
        ?string $message_effect_id = null,
        array|ReplyParameters|null $reply_parameters = null): Message
    {
        return new Message(...$this->bot('sendMediaGroup', [
            'chat_id' => $chat_id,
            'media' => $media,
            'business_connection_id' => $business_connection_id,
            'message_thread_id' => $message_thread_id,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
            'message_effect_id' => $message_effect_id,
            'reply_parameters' => $reply_parameters,
        ]));
    }

    public function sendLocation(string|int $chat_id,
        float $latitude,
        float $longitude,
        ?string $business_connection_id = null,
        ?float $horizontal_accuracy = null,
        ?int $message_thread_id = null,
        ?int $live_period = null,
        ?int $heading = null,
        ?int $proximity_alert_radius = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null,
        ?string $message_effect_id = null,
        array|ReplyParameters|null $reply_parameters = null,
        array|InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply|null $reply_markup = null): Message
    {
        return new Message(...$this->bot('sendLocation', [
            'chat_id' => $chat_id,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'business_connection_id' => $business_connection_id,
            'horizontal_accuracy' => $horizontal_accuracy,
            'message_thread_id' => $message_thread_id,
            'live_period' => $live_period,
            'heading' => $heading,
            'proximity_alert_radius' => $proximity_alert_radius,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
            'message_effect_id' => $message_effect_id,
            'reply_parameters' => $reply_parameters,
            'reply_markup' => $reply_markup,
        ]));
    }

    public function sendVenue(string|int $chat_id,
        float $latitude,
        float $longitude,
        string $title,
        string $address,
        ?string $business_connection_id = null,
        ?int $message_thread_id = null,
        ?string $foursquare_id = null,
        ?string $foursquare_type = null,
        ?string $google_place_id = null,
        ?string $google_place_type = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null,
        ?string $message_effect_id = null,
        array|ReplyParameters|null $reply_parameters = null,
        array|InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply|null $reply_markup = null): Message
    {
        return new Message(...$this->bot('sendVenue', [
            'chat_id' => $chat_id,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'title' => $title,
            'address' => $address,
            'business_connection_id' => $business_connection_id,
            'message_thread_id' => $message_thread_id,
            'foursquare_id' => $foursquare_id,
            'foursquare_type' => $foursquare_type,
            'google_place_id' => $google_place_id,
            'google_place_type' => $google_place_type,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
            'message_effect_id' => $message_effect_id,
            'reply_parameters' => $reply_parameters,
            'reply_markup' => $reply_markup,
        ]));
    }

    public function sendContact(string|int $chat_id,
        string $phone_number,
        string $first_name,
        ?string $business_connection_id = null,
        ?int $message_thread_id = null,
        ?string $last_name = null,
        ?string $vcard = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null,
        ?string $message_effect_id = null,
        array|ReplyParameters|null $reply_parameters = null,
        array|InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply|null $reply_markup = null): Message
    {
        return new Message(...$this->bot('sendContact', [
            'chat_id' => $chat_id,
            'phone_number' => $phone_number,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'vcard' => $vcard,
            'business_connection_id' => $business_connection_id,
            'message_thread_id' => $message_thread_id,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
            'message_effect_id' => $message_effect_id,
            'reply_parameters' => $reply_parameters,
            'reply_markup' => $reply_markup,
        ]));
    }

    public function sendPoll(string|int $chat_id,
        string $question,
        array $options,
        ?string $business_connection_id = null,
        ?int $message_thread_id = null,
        ?string $question_parse_mode = null,
        ?array $question_entities = null,
        ?bool $is_anonymous = null,
        ?string $type = null,
        ?bool $allows_multiple_answers = null,
        ?int $correct_option_id = null,
        ?string $explanation = null,
        ?string $explanation_parse_mode = null,
        ?array $explanation_entities = null,
        ?int $open_period = null,
        ?int $close_date = null,
        ?bool $is_closed = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null,
        ?string $message_effect_id = null,
        array|ReplyParameters|null $reply_parameters = null,
        array|InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply|null $reply_markup = null): Message
    {
        return new Message(...$this->bot('sendPoll', [
            'chat_id' => $chat_id,
            'question' => $question,
            'options' => $options,
            'question_parse_mode' => $question_parse_mode,
            'question_entities' => $question_entities,
            'business_connection_id' => $business_connection_id,
            'message_thread_id' => $message_thread_id,
            'is_anonymous' => $is_anonymous,
            'type' => $type,
            'allows_multiple_answers' => $allows_multiple_answers,
            'correct_option_id' => $correct_option_id,
            'explanation' => $explanation,
            'explanation_parse_mode' => $explanation_parse_mode,
            'explanation_entities' => $explanation_entities,
            'open_period' => $open_period,
            'close_date' => $close_date,
            'is_closed' => $is_closed,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
            'message_effect_id' => $message_effect_id,
            'reply_parameters' => $reply_parameters,
            'reply_markup' => $reply_markup,
        ]));
    }

    public function sendDice(string|int $chat_id,
        ?string $business_connection_id = null,
        ?int $message_thread_id = null,
        ?string $emoji = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null,
        ?string $message_effect_id = null,
        array|ReplyParameters|null $reply_parameters = null,
        array|InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply|null $reply_markup = null): Message
    {
        return new Message(...$this->bot('sendDice', [
            'chat_id' => $chat_id,
            'emoji' => $emoji,
            'business_connection_id' => $business_connection_id,
            'message_thread_id' => $message_thread_id,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
            'message_effect_id' => $message_effect_id,
            'reply_parameters' => $reply_parameters,
            'reply_markup' => $reply_markup,
        ]));
    }

    public function sendChatAction(string|int $chat_id,
        string $action,
        ?string $business_connection_id = null,
        ?int $message_thread_id = null): Message
    {
        return new Message(...$this->bot('sendChatAction', [
            'chat_id' => $chat_id,
            'action' => $action,
            'business_connection_id' => $business_connection_id,
            'message_thread_id' => $message_thread_id,
        ]));
    }

    public function setMessageReaction(string|int $chat_id,
        int $message_id,
        ?array $reaction = null,
        ?bool $is_big = null): Message
    {
        return new Message(...$this->bot('setMessageReaction', [
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'reaction' => $reaction,
            'is_big' => $is_big,
        ]));
    }

    public function answerCallbackQuery(string $callback_query_id,
        ?string $text = null,
        ?bool $show_alert = null,
        ?string $url = null,
        ?int $cache_time = null): Message
    {
        return new Message(...$this->bot('answerCallbackQuery', [
            'callback_query_id' => $callback_query_id,
            'text' => $text,
            'show_alert' => $show_alert,
            'url' => $url,
            'cache_time' => $cache_time,
        ]));
    }
}
