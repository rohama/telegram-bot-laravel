<?php

namespace Rohama\Telegram\Traits;

use Rohama\Telegram\Type\InlineQuery\InlineQueryResult;
use Rohama\Telegram\Type\InlineQuery\InlineQueryResultsButton;
use Rohama\Telegram\Type\InlineQuery\SentWebAppMessage;

trait inline
{
    public function answerInlineQuery(string $inline_query_id,
        array $results,
        ?int $cache_time = null,
        ?bool $is_personal = null,
        ?string $next_offset = null,
        array|InlineQueryResultsButton|null $button = null): bool
    {
        return $this->bot('answerInlineQuery', [
            'inline_query_id' => $inline_query_id,
            'results' => $results,
            'cache_time' => $cache_time,
            'is_personal' => $is_personal,
            'next_offset' => $next_offset,
            'button' => $button,
        ]);
    }

    public function answerWebAppQuery(string $web_app_query_id,
        array|InlineQueryResult $result): SentWebAppMessage
    {
        return new SentWebAppMessage(...$this->bot('answerWebAppQuery', [
            'web_app_query_id' => $web_app_query_id,
            'result' => $result,
        ]));
    }
}
