<?php

namespace Rohama\Telegram\Traits;

use Rohama\Telegram\Type\Chats\Bot\BotCommand;
use Rohama\Telegram\Type\Chats\Bot\BotCommandScope;
use Rohama\Telegram\Type\Chats\Bot\BotDescription;
use Rohama\Telegram\Type\Chats\Bot\BotName;
use Rohama\Telegram\Type\Chats\Bot\BotShortDescription;
use Rohama\Telegram\Type\Chats\Bot\MenuButton;
use Rohama\Telegram\Type\Chats\Business\BusinessConnection;
use Rohama\Telegram\Type\Chats\ChatAdministratorRights;
use Rohama\Telegram\Type\Chats\User;
use Rohama\Telegram\Type\Messages\Media\File;

trait bot
{
    public function getMe(): User
    {
        return new User(...$this->bot('getMe'));
    }

    public function logOut(): bool
    {
        return $this->bot('logOut');
    }

    public function close(): bool
    {
        return $this->bot('close');
    }

    public function getFile(string $file_id): File
    {
        return new File(...$this->bot('getFile', [
            'file_id' => $file_id,
        ]));
    }

    public function getBusinessConnection(string $business_connection_id): BusinessConnection
    {
        return new BusinessConnection(...$this->bot('getBusinessConnection', [
            'business_connection_id' => $business_connection_id,
        ]));
    }

    public function setMyCommands(array $commands,
        array|BotCommandScope|null $scope = null,
        ?string $language_code = null): bool
    {
        return $this->bot('setMyCommands', [
            'commands' => $commands,
            'scope' => $scope,
            'language_code' => $language_code,
        ]);
    }

    public function deleteMyCommands(array|BotCommandScope|null $scope = null,
        ?string $language_code = null): bool
    {
        return $this->bot('deleteMyCommands', [
            'scope' => $scope,
            'language_code' => $language_code,
        ]);
    }

    public function getMyCommands(array|BotCommandScope|null $scope = null,
        ?string $language_code = null): array
    {
        $commands = $this->bot('getMyCommands', [
            'scope' => $scope,
            'language_code' => $language_code,
        ]);

        return array_map(function ($command) {
            return new BotCommand(...$command);
        }, $commands);
    }

    public function setMyName(?string $name = null,
        ?string $language_code = null): bool
    {
        return $this->bot('setMyName', [
            'name' => $name,
            'language_code' => $language_code,
        ]);
    }

    public function getMyName(?string $language_code = null): BotName
    {
        return new BotName(...$this->bot('getMyName', [
            'language_code' => $language_code,
        ]));
    }

    public function setMyDescription(?string $description = null,
        ?string $language_code = null): bool
    {
        return $this->bot('setMyDescription', [
            'description' => $description,
            'language_code' => $language_code,
        ]);
    }

    public function getMyDescription(?string $language_code = null): BotDescription
    {
        return new BotDescription(...$this->bot('getMyDescription', [
            'language_code' => $language_code,
        ]));
    }

    public function setMyShortDescription(?string $short_description = null,
        ?string $language_code = null): bool
    {
        return $this->bot('setMyShortDescription', [
            'short_description' => $short_description,
            'language_code' => $language_code,
        ]);
    }

    public function getMyShortDescription(?string $language_code = null): BotShortDescription
    {
        return new BotShortDescription(...$this->bot('getMyShortDescription', [
            'language_code' => $language_code,
        ]));
    }

    public function setChatMenuButton(?int $chat_id = null,
        array|MenuButton|null $menu_button = null): bool
    {
        return $this->bot('setChatMenuButton', [
            'chat_id' => $chat_id,
            'menu_button' => $menu_button,
        ]);
    }

    public function getChatMenuButton(?int $chat_id = null): MenuButton
    {
        return new MenuButton(...$this->bot('getChatMenuButton', [
            'chat_id' => $chat_id,
        ]));
    }

    public function setMyDefaultAdministratorRights(array|ChatAdministratorRights|null $rights = null,
        ?bool $for_channels = null): bool
    {
        return $this->bot('setMyDefaultAdministratorRights', [
            'rights' => $rights,
            'for_channels' => $for_channels,
        ]);
    }

    public function getMyDefaultAdministratorRights(?bool $for_channels = null): ChatAdministratorRights
    {
        return new ChatAdministratorRights(...$this->bot('getMyDefaultAdministratorRights', [
            'for_channels' => $for_channels,
        ]));
    }
}
