<?php

namespace Rohama\Telegram\Traits;

use Rohama\Telegram\Type\InputFile;
use Rohama\Telegram\Type\Messages\ForceReply;
use Rohama\Telegram\Type\Messages\Keyboard\InlineKeyboardMarkup;
use Rohama\Telegram\Type\Messages\Keyboard\ReplyKeyboardMarkup;
use Rohama\Telegram\Type\Messages\Keyboard\ReplyKeyboardRemove;
use Rohama\Telegram\Type\Messages\Media\File;
use Rohama\Telegram\Type\Messages\ReplyParameters;
use Rohama\Telegram\Type\Sticker\InputSticker;
use Rohama\Telegram\Type\Sticker\MaskPosition;
use Rohama\Telegram\Type\Sticker\StickerSet;
use Rohama\Telegram\Type\Update\Message;

trait sticker
{
    public function sendSticker(string|int $chat_id,
        string|InputFile $sticker,
        ?string $business_connection_id = null,
        ?int $message_thread_id = null,
        ?string $emoji = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null,
        ?string $message_effect_id = null,
        array|ReplyParameters|null $reply_parameters = null,
        array|InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply|null $reply_markup = null): Message
    {
        return new Message(...$this->bot('sendSticker', [
            'chat_id' => $chat_id,
            'sticker' => $sticker,
            'business_connection_id' => $business_connection_id,
            'message_thread_id' => $message_thread_id,
            'emoji' => $emoji,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
            'message_effect_id' => $message_effect_id,
            'reply_parameters' => $reply_parameters,
            'reply_markup' => $reply_markup,
        ]));
    }

    public function getStickerSet(string $name): StickerSet
    {
        return new StickerSet(...$this->bot('getStickerSet', [
            'name' => $name,
        ]));
    }

    public function getCustomEmojiStickers(array $custom_emoji_ids): array
    {
        $stickers = $this->bot('getCustomEmojiStickers', [
            'custom_emoji_ids' => $custom_emoji_ids,
        ]);

        return array_map(function ($sticker) {
            return new \Rohama\Telegram\Type\Sticker\Sticker(...$sticker);
        }, $stickers);
    }

    public function uploadStickerFile(int $user_id,
        InputFile $sticker,
        string $sticker_format): File
    {
        return new File(...$this->bot('uploadStickerFile', [
            'user_id' => $user_id,
            'sticker' => $sticker,
            'sticker_format' => $sticker_format,
        ]));
    }

    public function createNewStickerSet(int $user_id,
        string $name,
        string $title,
        array $stickers,
        ?string $sticker_type = null,
        ?bool $needs_repainting = null): bool
    {
        return $this->bot('createNewStickerSet', [
            'user_id' => $user_id,
            'name' => $name,
            'title' => $title,
            'stickers' => $stickers,
            'sticker_type' => $sticker_type,
            'needs_repainting' => $needs_repainting,
        ]);
    }

    public function addStickerToSet(int $user_id,
        string $name,
        array|InputSticker $sticker): bool
    {
        return $this->bot('addStickerToSet', [
            'user_id' => $user_id,
            'name' => $name,
            'sticker' => $sticker,
        ]);
    }

    public function setStickerPositionInSet(string $sticker,
        int $position): bool
    {
        return $this->bot('setStickerPositionInSet', [
            'sticker' => $sticker,
            'position' => $position,
        ]);
    }

    public function deleteStickerFromSet(string $sticker): bool
    {
        return $this->bot('deleteStickerFromSet', [
            'sticker' => $sticker,
        ]);
    }

    public function replaceStickerInSet(int $user_id,
        string $name,
        string $old_sticker,
        array|InputSticker $sticker): bool
    {
        return $this->bot('replaceStickerInSet', [
            'user_id' => $user_id,
            'name' => $name,
            'old_sticker' => $old_sticker,
            'sticker' => $sticker,
        ]);
    }

    public function setStickerEmojiList(string $sticker,
        array $emoji_list): bool
    {
        return $this->bot('setStickerEmojiList', [
            'sticker' => $sticker,
            'emoji_list' => $emoji_list,
        ]);
    }

    public function setStickerKeywords(string $sticker,
        ?array $keywords = null): bool
    {
        return $this->bot('setStickerKeywords', [
            'sticker' => $sticker,
            'keywords' => $keywords,
        ]);
    }

    public function setStickerMaskPosition(string $sticker,
        array|MaskPosition|null $mask_position = null): bool
    {
        return $this->bot('setStickerMaskPosition', [
            'sticker' => $sticker,
            'mask_position' => $mask_position,
        ]);
    }

    public function setStickerSetTitle(string $name,
        string $title): bool
    {
        return $this->bot('setStickerSetTitle', [
            'name' => $name,
            'title' => $title,
        ]);
    }

    public function setStickerSetThumbnail(string $name,
        int $user_id,
        string|InputFile|null $thumbnail = null,
        string $format): bool
    {
        return $this->bot('setStickerSetThumbnail', [
            'name' => $name,
            'user_id' => $user_id,
            'thumbnail' => $thumbnail,
            'format' => $format,
        ]);
    }

    public function setCustomEmojiStickerSetThumbnail(string $name,
        ?string $custom_emoji_id = null): bool
    {
        return $this->bot('setCustomEmojiStickerSetThumbnail', [
            'name' => $name,
            'custom_emoji_id' => $custom_emoji_id,
        ]);
    }

    public function deleteStickerSet(string $name): bool
    {
        return $this->bot('deleteStickerSet', [
            'name' => $name,
        ]);
    }
}
