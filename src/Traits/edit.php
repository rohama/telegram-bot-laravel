<?php

namespace Rohama\Telegram\Traits;

use Rohama\Telegram\Type\Messages\Keyboard\InlineKeyboardMarkup;
use Rohama\Telegram\Type\Messages\LinkPreviewOptions;
use Rohama\Telegram\Type\Messages\Media\InputMedia;
use Rohama\Telegram\Type\Messages\Poll\Poll;
use Rohama\Telegram\Type\Update\Message;

trait edit
{
    public function editMessageText(string $text,
        string|int|null $chat_id = null,
        ?int $message_id = null,
        ?string $inline_message_id = null,
        ?string $business_connection_id = null,
        ?string $parse_mode = null,
        ?array $entities = null,
        array|LinkPreviewOptions|null $link_preview_options = null,
        array|InlineKeyboardMarkup|null $reply_markup = null): Message|bool
    {
        $message = $this->bot('editMessageText', [
            'text' => $text,
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'inline_message_id' => $inline_message_id,
            'business_connection_id' => $business_connection_id,
            'parse_mode' => $parse_mode,
            'entities' => $entities,
            'link_preview_options' => $link_preview_options,
            'reply_markup' => $reply_markup,
        ]);

        return is_array($message) ? new Message(...$message) : $message;
    }

    public function editMessageCaption(?string $caption = null,
        string|int|null $chat_id = null,
        ?int $message_id = null,
        ?string $inline_message_id = null,
        ?string $business_connection_id = null,
        ?string $parse_mode = null,
        ?array $caption_entities = null,
        ?bool $show_caption_above_media = null,
        array|InlineKeyboardMarkup|null $reply_markup = null): Message|bool
    {
        $message = $this->bot('editMessageCaption', [
            'caption' => $caption,
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'inline_message_id' => $inline_message_id,
            'business_connection_id' => $business_connection_id,
            'parse_mode' => $parse_mode,
            'entities' => $caption_entities,
            'show_caption_above_media' => $show_caption_above_media,
            'reply_markup' => $reply_markup,
        ]);

        return is_array($message) ? new Message(...$message) : $message;
    }

    public function editMessageMedia(array|InputMedia $media,
        string|int|null $chat_id = null,
        ?int $message_id = null,
        ?string $inline_message_id = null,
        ?string $business_connection_id = null,
        array|InlineKeyboardMarkup|null $reply_markup = null): Message|bool
    {
        $message = $this->bot('editMessageMedia', [
            'media' => $media,
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'inline_message_id' => $inline_message_id,
            'business_connection_id' => $business_connection_id,
            'reply_markup' => $reply_markup,
        ]);

        return is_array($message) ? new Message(...$message) : $message;
    }

    public function editMessageLiveLocation(float $latitude,
        float $longitude,
        string|int|null $chat_id = null,
        ?int $message_id = null,
        ?string $inline_message_id = null,
        ?string $business_connection_id = null,
        ?int $live_period = null,
        ?float $horizontal_accuracy = null,
        ?int $heading = null,
        ?int $proximity_alert_radius = null,
        array|InlineKeyboardMarkup|null $reply_markup = null): Message|bool
    {
        $message = $this->bot('editMessageLiveLocation', [
            'latitude' => $latitude,
            'longitude' => $longitude,
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'inline_message_id' => $inline_message_id,
            'business_connection_id' => $business_connection_id,
            'live_period' => $live_period,
            'horizontal_accuracy' => $horizontal_accuracy,
            'heading' => $heading,
            'proximity_alert_radius' => $proximity_alert_radius,
            'reply_markup' => $reply_markup,
        ]);

        return is_array($message) ? new Message(...$message) : $message;
    }

    public function stopMessageLiveLocation(string|int|null $chat_id = null,
        ?int $message_id = null,
        ?string $inline_message_id = null,
        ?string $business_connection_id = null,
        array|InlineKeyboardMarkup|null $reply_markup = null): Message|bool
    {
        $message = $this->bot('stopMessageLiveLocation', [
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'inline_message_id' => $inline_message_id,
            'business_connection_id' => $business_connection_id,
            'reply_markup' => $reply_markup,
        ]);

        return is_array($message) ? new Message(...$message) : $message;
    }

    public function editMessageReplyMarkup(string|int|null $chat_id = null,
        ?int $message_id = null,
        ?string $inline_message_id = null,
        ?string $business_connection_id = null,
        array|InlineKeyboardMarkup|null $reply_markup = null): Message|bool
    {
        $message = $this->bot('editMessageReplyMarkup', [
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'inline_message_id' => $inline_message_id,
            'business_connection_id' => $business_connection_id,
            'reply_markup' => $reply_markup,
        ]);

        return is_array($message) ? new Message(...$message) : $message;
    }

    public function stopPoll(string|int $chat_id,
        int $message_id,
        ?string $business_connection_id = null,
        array|InlineKeyboardMarkup|null $reply_markup = null): Poll
    {
        return new Poll(...$this->bot('stopPoll', [
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'business_connection_id' => $business_connection_id,
            'reply_markup' => $reply_markup,
        ]));
    }

    public function deleteMessage(string|int $chat_id,
        int $message_id): bool
    {
        return $this->bot('deleteMessage', [
            'chat_id' => $chat_id,
            'message_id' => $message_id,
        ]);
    }

    public function deleteMessages(string|int $chat_id,
        array $message_ids): bool
    {
        return $this->bot('deleteMessages', [
            'chat_id' => $chat_id,
            'message_ids' => $message_ids,
        ]);
    }
}
