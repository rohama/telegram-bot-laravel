<?php

namespace Rohama\Telegram\Traits;

use Rohama\Telegram\Type\Game\GameHighScore;
use Rohama\Telegram\Type\Messages\ForceReply;
use Rohama\Telegram\Type\Messages\Keyboard\InlineKeyboardMarkup;
use Rohama\Telegram\Type\Messages\Keyboard\ReplyKeyboardMarkup;
use Rohama\Telegram\Type\Messages\Keyboard\ReplyKeyboardRemove;
use Rohama\Telegram\Type\Messages\ReplyParameters;
use Rohama\Telegram\Type\Update\Message;

trait game
{
    public function sendGame(string|int $chat_id,
        string $game_short_name,
        ?string $business_connection_id = null,
        ?int $message_thread_id = null,
        ?bool $disable_notification = null,
        ?bool $protect_content = null,
        ?string $message_effect_id = null,
        array|ReplyParameters|null $reply_parameters = null,
        array|InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply|null $reply_markup = null): Message
    {
        return new Message(...$this->bot('sendGame', [
            'chat_id' => $chat_id,
            'game_short_name' => $game_short_name,
            'business_connection_id' => $business_connection_id,
            'message_thread_id' => $message_thread_id,
            'disable_notification' => $disable_notification,
            'protect_content' => $protect_content,
            'message_effect_id' => $message_effect_id,
            'reply_parameters' => $reply_parameters,
            'reply_markup' => $reply_markup,
        ]));
    }

    public function setGameScore(int $user_id,
        int $score,
        ?bool $force = null,
        ?bool $disable_edit_message = null,
        ?int $chat_id = null,
        ?int $message_id = null,
        ?string $inline_message_id = null): Message|bool
    {
        $message = $this->bot('setGameScore', [
            'user_id' => $user_id,
            'score' => $score,
            'force' => $force,
            'disable_edit_message' => $disable_edit_message,
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'inline_message_id' => $inline_message_id,
        ]);

        return is_array($message) ? new Message(...$message) : $message;
    }

    public function getGameHighScores(int $user_id,
        ?int $chat_id = null,
        ?int $message_id = null,
        ?string $inline_message_id = null): array
    {
        $highscores = $this->bot('getGameHighScores', [
            'user_id' => $user_id,
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'inline_message_id' => $inline_message_id,
        ]);

        return array_map(function ($highscore) {
            return new GameHighScore(...$highscore);
        }, $highscores);
    }
}
