<?php

namespace Rohama\Telegram\Traits;

use Rohama\Telegram\Type\InputFile;
use Rohama\Telegram\Type\Update\WebhookInfo;

trait update
{
    public function getUpdates(?int $offset = null,
        ?int $limit = null,
        ?int $timeout = null,
        ?array $allowed_updates = null): array
    {
        $updates = $this->bot('getUpdates', [
            'offset' => $offset,
            'limit' => $limit,
            'timeout' => $timeout,
            'allowed_updates' => $allowed_updates,
        ]);

        return array_map(function ($update) {
            return new \Rohama\Telegram\Type\Update\Update(...$update);
        }, $updates);
    }

    public function setWebhook(string $url,
        ?InputFile $certificate = null,
        ?string $ip_address = null,
        ?int $max_connections = null,
        ?array $allowed_updates = null,
        ?bool $drop_pending_updates = null,
        ?string $secret_token = null): bool
    {
        return $this->bot('setWebhook', [
            'url' => $url,
            'certificate' => $certificate,
            'ip_address' => $ip_address,
            'max_connections' => $max_connections,
            'allowed_updates' => $allowed_updates,
            'drop_pending_updates' => $drop_pending_updates,
            'secret_token' => $secret_token,
        ]);
    }

    public function deleteWebhook(?bool $drop_pending_updates = null): bool
    {
        return $this->bot('deleteWebhook', [
            'drop_pending_updates' => $drop_pending_updates,
        ]);
    }

    public function getWebhookInfo(): WebhookInfo
    {
        return new WebhookInfo(...$this->bot('getWebhookInfo'));
    }
}
